# Terraform demonstration

This Terraform module creates up to two kind of VMs:
 - public facing bastion VM
 - private HPC VM farm

Cloud-init add following:
 - Add ssh keys, disable SSH password auth
 - Create partition and filesystemand mount extra data from extra volume

## Infrastructure schema

### Two tier infrastructure: public bastion and private VM farm

![two-tier-infra.png](/clouds/common/pictures/two-tier-infra.png)

### Single tier infrastructure: public VM farm

![single-tier-infra.png](/clouds/common/pictures/single-tier-infra.png)

## Create Infrastructure

1. Clone the repository.
1. Load you OpenStack application credentials to environment variables `source project_openrc.sh.inc`
1. Override any infrastructure variables in [main.tf](main.tf) file if needed. Full set of variables can be found in [modules/2tier_public_bastion_private_vm_farm/variables.tf](modules/2tier_public_bastion_private_vm_farm/variables.tf) or [modules/1tier-public-vm-farm/variables.tf](modules/1tier-public-vm-farm/variables.tf).
1. In the [terraform root directory](/clouds/g1/brno/vo.enes.org/terraform) run following commands to initiate and validate environment
   * `terraform init`
   * `terraform validate`
1. In the [same directory](/clouds/g1/brno/vo.enes.org/terraform) run commands to deploy cloud infrastructure
   * `terraform plan --out plan`
   * `terraform apply plan`
1. Once you need to change the infrastructure, first modify the infrastructure declaration and repeat above steps to deploy changes.
1. Similarly for resource teardown, once you want to clean-up cloud resources issue `terraform destroy`.


Detailed terminal transcript can be found in [terminal-transcript.log](./terminal-transcript.log).


## Access to the HPC nodes

In single tier infrastructure you access directly the individual HPC VM nodes via SSH on public IP addresses.
Two tier infrastructure requires the access following way:
1. Establish the connection with bastion
```sh
sshuttle -r ubuntu@<bastion-ip>
```
1. Connect directly to HPC VM nodes via SSH on private IP addresses:
```sh
ssh  ubuntu@<vm-node-ip-from-10.10.10.0/24>
```
