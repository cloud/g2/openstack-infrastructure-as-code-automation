resource "openstack_networking_network_v2" "network_default" {
  name           = "${var.infra_name}_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_default" {
  name            = "${var.infra_name}_subnet"
  network_id      = openstack_networking_network_v2.network_default.id
  cidr            = var.int_network
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "8.8.8.8"]
}

data "openstack_networking_network_v2" "external_network" {
  name = var.public_external_network
}

resource "openstack_networking_router_v2" "router_default" {
  name                = "${var.infra_name}_infra-test"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.external_network.id
}

resource "openstack_networking_router_interface_v2" "router_default_interface" {
  router_id = openstack_networking_router_v2.router_default.id
  subnet_id = openstack_networking_subnet_v2.subnet_default.id
}

resource "openstack_networking_port_v2" "nodes_ports" {
  count              = var.nodes_count
  name               = "${var.infra_name}_${var.nodes_name}_port_${count.index+1}"
  network_id         = openstack_networking_network_v2.network_default.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default.id
  }
}
