
resource "openstack_compute_keypair_v2" "pubkey" {
  name       = "${var.infra_name}-keypair"
  public_key = file("${var.ssh_public_key}")
}

