terraform {
  backend "local" {}
}

module "toplevel" {
  # two tier infrastructure (2tier_public_bastion_private_vm_farm module):
  # * single public facing tiny bastion VM
  # * <nodes_count> private HPC VM farm
  source                  = "./modules/2tier_public_bastion_private_vm_farm"
  # single tier infrastructure (1tier_public_vm_farm monule)
  # * <nodes_count> public HPC VM farm
  #source                  = "./modules/1tier_public_vm_farm"

  infra_name              = "vo-enes-org-tf-demo"

  nodes_count             = 3
  nodes_extra_volume_size = 1000  # in GB

  #nodes_flavor            = "hpc.16core-32ram"
  #nodes_image             = "ubuntu-jammy-x86_64"
}
