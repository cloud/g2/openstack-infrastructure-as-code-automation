# vo.enes.org IaaS infrastructure demo

Project `vo.enes.org` in `egi_eu` domain is granted and users may log in to [MetaCentrum OpenStack cloud dashboard](https://cloud.metacentrum.cz/) using EGI Check-in authentication.

We recommend to build custom cloud infrastructure with Terraform or openstack client rather than using [MetaCentrum OpenStack cloud Horizon UI dashboard](https://dashboard.cloud.muni.cz).

To use huge amount of block and object storage reserved for the `vo.enes.org` project you need to explicitly use dedicated OpenStack volume type `ceph-extra-ec`. Below demos show in detail how to do so.

## [Terraform `vo.enes.org` demo](./terraform)

Terraform `vo.enes.org` demo shows how to automate building highly scalable IaaS infrastructure.

## [OpenStack client `vo.enes.org` demo](./commandline)

OpenStack shell script `vo.enes.org` demo shows how to automate small IaaS infrastructure which does not need additional scalability.
