# Build and maintain OpenStack infrastructure from command-line using terraform

This Terraform module creates simple virtual infrastructure cluster in OpenStack.

## Pre-requisites
 * Linux/Mac/WSL2 terminal
 * installed terraform ([how?](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli#install-terraform))
 * downloaded application credentials from OpenStack Horizon dashboard ([how?](https://docs.cloud.muni.cz/cloud/cli/#getting-credentials))

## Create Infrastructure

1. Clone the repository.
1. Load you OpenStack application credentials to environment variables `source ~/conf/prod-meta-cloud-new-openstack-all-roles-openrc.sh`
1. Override any variable if needed. Every variable specified in [modules/infra/variables.tf](modules/infra/variables.tf) can be overridden in the [main.tf](main.tf) file in its *module* section.
1. In the root folder run `terraform init`.
1. In the root folder run `terraform validate`.
1. Run `terraform plan -out plan1` to generate terraform plan.
1. Run `terraform apply "plan1"` to apply the plan.

## Destroy Infrastructure

To delete all created resources run the following commands:

```sh
terraform plan -destroy -out plan1
terraform apply "plan1"
```

## SSH to nodes

### node with associated FIP
Connecting to master is as easy as `ssh ubuntu@<any-master-ip>`

### other nodes
Establish SSH VPN to first head node using `sshuttle`.

```sh
sshuttle -r ubuntu@<any-master-ip> 192.168.0.0/24
```

Connection to any other node is then via SSH VPN (sshuttle) on internal network i.e. `ssh ubuntu@<vm-internal-ip-from-192.168.0.0/24>`
