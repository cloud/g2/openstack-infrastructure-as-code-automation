output "node_instance_ip" {
  value = openstack_compute_instance_v2.nodes[*].access_ip_v4
}

output "node_fip" {
  value = openstack_networking_floatingip_v2.fip.address
}