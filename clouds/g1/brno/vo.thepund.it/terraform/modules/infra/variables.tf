variable "kusername" {
  description = "Name prefix for all resources. Use a-z, 0-9 and the hyphen (-) only."
  default     = "demo"
}

variable "public_key" {
  default = "~/.ssh/id_rsa.pub"
}

##################
# nodes settings #
##################
variable "nodes_count" {
  default = 3
}

variable "nodes_name_prefix" {
  description = "Use a-z, 0-9 and the hyphen (-) only."
  default = ""
}

variable "nodes_flavor" {
  default = "hpc.8core-32ram-ssd-ephem"
}


variable "int_network" {
  description = "Internal network address, use CIDR notation"
  default     = "10.0.0.0/24"
}

variable "pool" {
  description = "FIP pool"
  default     = "public-cesnet-195-113-167-GROUP"
}

variable "image" {
  description = "Image used for virtual nodes"
  default     = "88f8e72a-bbf0-4ccc-8ff2-4f3188cd0d18"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

#########################
# node volumes
#########################

variable "node_volumes_count" {
  description = "Number of volumes added to nodes (allowed values: 0 to disable attaching volumes, 1 single volume to attach)"
  default     = ""
}

variable "node_volume_size" {
  description = "Size of volume attached to nodes (in GB)"
  default     = 500
}
