
resource "openstack_blockstorage_volume_v3" "volumes_b" {
  count = var.node_volumes_count != "" ? var.node_volumes_count : var.nodes_count
  name  = "${var.kusername}-node-volume-b-${count.index+1}"
  size  = var.node_volume_size
}

resource "openstack_compute_volume_attach_v2" "volumes_b_attachments" {
  count = var.node_volumes_count != "" ? var.node_volumes_count : var.nodes_count
  instance_id = element(openstack_compute_instance_v2.nodes.*.id, count.index)
  volume_id   = element(openstack_blockstorage_volume_v3.volumes_b.*.id, count.index)
  #device = "/dev/sdb"
}

