
resource "openstack_compute_keypair_v2" "pubkey" {
  name       = "${var.kusername}-demo"
  public_key = file("${var.public_key}")
}

