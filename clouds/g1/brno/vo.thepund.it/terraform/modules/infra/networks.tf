###############################################################
# Define networking                                           #
# Security group rules are in separate file secgroup_rules.tf #
###############################################################

resource "openstack_networking_network_v2" "network_default" {
  name           = "${var.kusername}_demo"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_default" {
  name            = "${var.kusername}_demo"
  network_id      = openstack_networking_network_v2.network_default.id
  cidr            = var.int_network
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "8.8.8.8"]
}

data "openstack_networking_network_v2" "terraform-demo-external-net" {
  name = var.pool
}

resource "openstack_networking_router_v2" "router_default" {
  name                = "${var.kusername}_demo"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.terraform-demo-external-net.id
}

resource "openstack_networking_router_interface_v2" "terraform-demo-router-interface-1" {
  router_id = openstack_networking_router_v2.router_default.id
  subnet_id = openstack_networking_subnet_v2.subnet_default.id
}

# Floating IPs (only for single (first) node)
resource "openstack_networking_floatingip_v2" "fip" {
  pool  = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "res_fip_associate" {
  floating_ip = openstack_networking_floatingip_v2.fip.address
  instance_id = openstack_compute_instance_v2.nodes[0].id
}

# Ports
resource "openstack_networking_port_v2" "ports" {
  count              = var.nodes_count
  name               = "${var.kusername}_port_${count.index+1}"
  network_id         = openstack_networking_network_v2.network_default.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default.id
  }
}