terraform {
  backend "local" {}
}

terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }
}


provider "openstack" {
  # auth arguments are read from environment variables (sourced opestack RC file)
  auth_url = "https://identity.cloud.muni.cz/v3"
}


module "demo" {
  source = "./modules/infra"

  # Example of variable override
  nodes_count        = 1
  kusername          = "thepundit"
  public_key         = "~/.ssh/id_rsa.pub"

  nodes_flavor = "standard.2core-16ram"
  image = "ubuntu-jammy-x86_64"

  int_network = "192.168.0.0/24"
  pool = "public-muni-147-251-124-GROUP"

  # attach additional single volume 500GB
  node_volumes_count = 1
  node_volume_size = 500
}

