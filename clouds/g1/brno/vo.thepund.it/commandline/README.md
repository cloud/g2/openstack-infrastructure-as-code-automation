# Build OpenStack infrastructure from command-line using openstack client

## Pre-requisites
 * Linux/Mac/WSL2 terminal
 * installed openstack client ([how?](https://docs.fuga.cloud/how-to-use-the-openstack-cli-tools-on-linux))
 * downloaded application credentials from OpenStack Horizon dashboard ([how?](https://docs.cloud.muni.cz/cloud/cli/#getting-credentials))


## How to use the script
```sh
./cmdline-demo-group-project.sh "infrastructure-a"
```

## Infrastructure schema

![obrazek.png](./obrazek.png)
