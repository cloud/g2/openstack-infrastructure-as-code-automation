# vo.thepund.it Infrastructure as Code demo

There are presented two simple approaches:
 * [infrastructure using terraform (best practice)](./terraform)
 * [infrastructure using command-line openstack client](./commandline)
