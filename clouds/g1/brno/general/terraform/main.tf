terraform {
  backend "local" {}
}

module "toplevel" {
  # two tier infrastructure (2tier_public_bastion_private_vm_farm module):
  # * single public facing tiny bastion VM
  # * <nodes_count> private HPC VM farm
  source                  = "./modules/2tier_public_bastion_private_vm_farm"
  # single tier infrastructure (1tier_public_vm_farm monule)
  # * <nodes_count> public HPC VM farm
  #source                  = "./modules/1tier_public_vm_farm"

  infra_name              = "general-tf-demo"

  nodes_count             = 2
  nodes_extra_volume_size = 20  # in GB

  #nodes_flavor            = "standard.medium"
  #nodes_image             = "ubuntu-jammy-x86_64"
}
