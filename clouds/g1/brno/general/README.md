# General IaaS infrastructure demo

Assuming you are added into a group project and you can log in via [MetaCentrum OpenStack cloud dashboard](https://cloud.metacentrum.cz/) using one of supported federations (e-INFRA CZ, EGI CHeck-in, ...).

We recommend to build custom cloud infrastructure with Terraform or openstack client rather than using [MetaCentrum OpenStack cloud Horizon UI dashboard](https://dashboard.cloud.muni.cz).

Below demos show in detail how to do so.

## [Terraform `general` demo](./terraform)

Terraform demo shows how to automate building highly scalable IaaS infrastructure.

## [OpenStack client `general` demo](./commandline)

OpenStack shell script demo shows how to automate small IaaS infrastructure which does not need additional scalability.
