#!/usr/bin/env bash
# general project command-line demo
# Usage: cmdline-demo.sh [ostack-entities-prefix/profile-name]
#

SCRIPT_DIR=$(dirname $(readlink -f $0))
#############################################################################
# variables
#############################################################################
CLOUD_ENV_NAME="g1-production"
OPENSTACK_BIN="${OPENSTACK_BIN:-openstack}"
ENTITIES_PREFIX="${1:-"${USER}_$(hostname)"}"
EXTERNAL_NETWORK_NAME="public-muni-147-251-124-GROUP"
KEYPAIR_NAME="${ENTITIES_PREFIX}-demo-keypair"
NETWORK_NAME="${ENTITIES_PREFIX}-demo-network"
SUBNET_NAME="${ENTITIES_PREFIX}-demo-subnet"
SUBNET_CIDR="${SUBNET_CIDR:-"192.168.221.0/24"}"
SERVER_NAME="${ENTITIES_PREFIX}-demo-server"
FLAVOR_NAME="${FLAVOR_NAME:-"standard.small"}"
IMAGE_NAME="${IMAGE_NAME:-"ubuntu-jammy-x86_64"}"
VM_LOGIN="${VM_LOGIN:-"ubuntu"}"
ROUTER_NAME="${ENTITIES_PREFIX}-demo-router"
FIP_FILE="${ENTITIES_PREFIX}-demo-fip.txt"
SECGROUP_NAME="${ENTITIES_PREFIX}-demo-secgroup"
SSH_KEYPAIR_DIR="${HOME}/.ssh/generated-keypair"
EXTRA_VOLUME_SIZE_GB=${EXTRA_VOLUME_SIZE_GB:-"10"}
EXTRA_VOLUME_NAME="${ENTITIES_PREFIX}-demo-volume"
EXTRA_VOLUME_TYPE="${EXTRA_VOLUME_TYPE:-"ceph-standard"}"
SERVER_CREATE_ADDITIONAL_ARGS="${SERVER_CREATE_ADDITIONAL_ARGS:-""}"
SERVER_EPHEMERAL_DISK_SIZE="${SERVER_EPHEMERAL_DISK_SIZE:-"0"}"
#############################################################################
# functions
#############################################################################
source ${SCRIPT_DIR}/../../../../common/lib.sh.inc

#############################################################################
# main steps
#############################################################################
log "Using commandline tools:"
report_tools || myexit 1

log "Using OpenStack cloud:"
${OPENSTACK_BIN} version show -fcsv | grep identity || myexit 1
log "In project $(is_personal_project)"

# delete objects (from previous run)
log "Delete previously created objects in profile ${ENTITIES_PREFIX} (so we start from scratch)"
delete_objects_group_project

log "List currently allocated objects (profile ${ENTITIES_PREFIX})"
list_objects

log_keypress "Create (generate) locally SSH keypair, upload public SSH key to cloud"
mkdir -p "${SSH_KEYPAIR_DIR}"
chmod 700 "${SSH_KEYPAIR_DIR}"
if [ -s "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}" -a -s "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}.pub" ]; then
  log "Reusing already existing SSH keypair at ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}"
else
  ssh-keygen -t rsa -b 4096 -f "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}"
fi
ssh-keygen -t rsa -b 4096 -f "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}"
${OPENSTACK_BIN} keypair create --type ssh --public-key "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}.pub" "${KEYPAIR_NAME}"
ls -la ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}*
# ---------------------------------------------------------------------------

log_keypress "Create cloud security groups (custom VM firewall) to allow outgoing traffic and incomming SSH traffic on port 22"
${OPENSTACK_BIN} security group create --description "${ENTITIES_PREFIX} demo default security group" "${SECGROUP_NAME}"
${OPENSTACK_BIN} security group rule create --ingress --proto tcp --remote-ip 0.0.0.0/0 --dst-port 22 "${SECGROUP_NAME}"
${OPENSTACK_BIN} security group rule create --egress --proto tcp --remote-ip 0.0.0.0/0 --dst-port 1:65535 "${SECGROUP_NAME}"
# ---------------------------------------------------------------------------

log_keypress "Create cloud private network and subnet, so far isolated (CIDR:${SUBNET_CIDR})"
${OPENSTACK_BIN} network create "${NETWORK_NAME}"
NETWORK_ID=$(${OPENSTACK_BIN} network show "${NETWORK_NAME}" -f value -c id)
${OPENSTACK_BIN} subnet create "${SUBNET_NAME}" --network "${NETWORK_ID}" --subnet-range "${SUBNET_CIDR}"
# ---------------------------------------------------------------------------

if [ "${EXTRA_VOLUME_SIZE_GB}" -gt 0 ]; then
  log_keypress "Create cloud VM extra volume \"${EXTRA_VOLUME_NAME}\" with following configuration:\n" \
               "  size: ${EXTRA_VOLUME_SIZE_GB} GB, volume type: ${EXTRA_VOLUME_TYPE}"
  ${OPENSTACK_BIN} volume create --type "${EXTRA_VOLUME_TYPE}" --size "${EXTRA_VOLUME_SIZE_GB}" ${EXTRA_VOLUME_NAME}
fi
# ---------------------------------------------------------------------------

if [ -n "${SERVER_EPHEMERAL_DISK_SIZE}" -a "${SERVER_EPHEMERAL_DISK_SIZE}" -gt "0" ]; then
  SERVER_CREATE_ADDITIONAL_ARGS="${SERVER_CREATE_ADDITIONAL_ARGS} --ephemeral=size=${SERVER_EPHEMERAL_DISK_SIZE}"
fi
log_keypress "Create cloud VM instance \"${SERVER_NAME}\" with following configuration:\n" \
             "  flavor: ${FLAVOR_NAME}, image/os: ${IMAGE_NAME}, network: ${NETWORK_NAME}\n" \
             "  keypair: ${KEYPAIR_NAME}, sec-group/firewall: ${SECGROUP_NAME})" \
             "  additional arguments: ${SERVER_CREATE_ADDITIONAL_ARGS}"
${OPENSTACK_BIN} server create --flavor "${FLAVOR_NAME}" --image "${IMAGE_NAME}" \
                        --network "${NETWORK_ID}" --key-name "${KEYPAIR_NAME}" \
                        --security-group "${SECGROUP_NAME}" ${SERVER_CREATE_ADDITIONAL_ARGS} "${SERVER_NAME}"
SERVER_ID=$(${OPENSTACK_BIN} server show "${SERVER_NAME}" -f value -c id)

log "Wait for VM instance \"${SERVER_NAME}\" being ACTIVE"
vm_wait_for_status "${SERVER_NAME}" "ACTIVE"

if [ "${EXTRA_VOLUME_SIZE_GB}" -gt 0 ]; then
  log_keypress "Attach extra volume \"${EXTRA_VOLUME_NAME}\" (${EXTRA_VOLUME_SIZE_GB} GB) to VM \"${SERVER_NAME}\""
  ${OPENSTACK_BIN} server add volume ${SERVER_NAME} ${EXTRA_VOLUME_NAME} --device /dev/sdb
fi
# ---------------------------------------------------------------------------

log "Route VM from internal software defined networking outside"
log_keypress "  1] Create route, associate router with external provider network and internal subnet (${SUBNET_CIDR})"
${OPENSTACK_BIN} router create "${ROUTER_NAME}"
${OPENSTACK_BIN} router set "${ROUTER_NAME}" --external-gateway "${EXTERNAL_NETWORK_NAME}"
${OPENSTACK_BIN} router add subnet "${ROUTER_NAME}" "${SUBNET_NAME}"

log_keypress "  2] Allocate single FIP (floating ip) from external provider network"
FIP=$(${OPENSTACK_BIN} floating ip create "${EXTERNAL_NETWORK_NAME}" -f value -c name)
echo "${FIP}" > "${FIP_FILE}"
echo "Obtained public FIP ${FIP}"

log_keypress "  3] Assign selected FIP with created VM"
${OPENSTACK_BIN} server add floating ip "${SERVER_NAME}" "${FIP}"

log "Test access to the VM server instance"
log_keypress "  1] TCP ping (ncat -z ${FIP} 22)"
test_vm_access "${FIP}"
log_keypress "  2] SSH command (ssh -i ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME} ${VM_LOGIN}@${FIP})"
ssh-keygen -R ${FIP} &>/dev/null
ssh -o StrictHostKeyChecking=no -i "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}" "${VM_LOGIN}@${FIP}" 'echo "";uname -a;uptime; echo "VM access succeeded!"'

log_keypress "Object summary in profile ${ENTITIES_PREFIX}"
list_objects
# ---------------------------------------------------------------------------

log_keypress "Teardown of the objects " \
             "(Interrupt with CTRL-C if you want to keep the created infrastructure and skip its destruction)"
delete_objects_group_project
