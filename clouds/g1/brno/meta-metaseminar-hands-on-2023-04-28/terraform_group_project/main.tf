terraform {
  backend "local" {}
}

terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }
}


provider "openstack" {
  # auth arguments are read from environment variables (sourced opestack RC file)
  auth_url = "https://identity.cloud.muni.cz/v3"
}


module "demo" {
  source = "./modules/infra"

  # Example of variable override
  nodes_count        = 1
  kusername          = "metacentrum-seminar-hands-on"
  public_key         = "~/.ssh/id_rsa.pub"

  nodes_flavor = "standard.small"
  image = "ubuntu-jammy-x86_64"

  int_network = "192.168.0.0/24"
  pool = "public-muni-147-251-124-GROUP"

  # attach additional single volumes
  sdb_volume = 1        # 0/1 absent/present
  sdb_volume_size = 1   # 1GB
  sdc_volume = 0        # 0/1 absent/present
  sdc_volume_size = 2   # 2GB
}

