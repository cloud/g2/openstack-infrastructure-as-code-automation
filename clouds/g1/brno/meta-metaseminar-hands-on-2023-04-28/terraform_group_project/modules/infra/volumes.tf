
# B volume[s]
resource "openstack_blockstorage_volume_v3" "volumes_b" {
  count = var.sdb_volume > 0 ? var.nodes_count : 0
  name  = "${var.kusername}-node-volume-b-${count.index+1}"
  size  = var.sdb_volume_size
}

resource "openstack_compute_volume_attach_v2" "volumes_b_attachments" {
  count = var.sdb_volume > 0 ? var.nodes_count : 0
  instance_id = element(openstack_compute_instance_v2.nodes.*.id, count.index)
  volume_id   = element(openstack_blockstorage_volume_v3.volumes_b.*.id, count.index)
  device = "/dev/sdb"
}

# C volume[s]
resource "openstack_blockstorage_volume_v3" "volumes_c" {
  count = var.sdc_volume > 0 ? var.nodes_count : 0
  name  = "${var.kusername}-node-volume-c-${count.index+1}"
  size  = var.sdc_volume_size
}

resource "openstack_compute_volume_attach_v2" "volumes_c_attachments" {
  count = var.sdc_volume > 0 ? var.nodes_count : 0
  instance_id = element(openstack_compute_instance_v2.nodes.*.id, count.index)
  volume_id   = element(openstack_blockstorage_volume_v3.volumes_c.*.id, count.index)
  device = "/dev/sdc"
}


