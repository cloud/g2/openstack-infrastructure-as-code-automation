
####################
# Define instances #
####################

resource "openstack_compute_instance_v2" "nodes" {
  count           = var.nodes_count
  name            = "${var.kusername}-${var.nodes_name_prefix}-${count.index+1}"
  image_name      = var.image
  flavor_name     = var.nodes_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.kusername}-${var.nodes_name_prefix}-${count.index+1}.local\n${file("${path.module}/cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.network_default.id
    port = element(openstack_networking_port_v2.ports.*.id, count.index)
  }

}
