#!/usr/bin/env bash
# MetaCentrum MCC openstack command-line demo - group projecp
# Usage: cmdline-demo-group-project.sh [ostack-entities-prefix]
#

SCRIPT_DIR=$(dirname $(readlink -f $0))
#############################################################################
# variables
#############################################################################
ENTITIES_PREFIX="${1:-"${USER}_$(hostname)"}"
EXTERNAL_NETWORK_NAME="public-muni-147-251-124-GROUP"
KEYPAIR_NAME="${ENTITIES_PREFIX}-demo-keypair"
NETWORK_NAME="${ENTITIES_PREFIX}-demo-network"
SUBNET_NAME="${ENTITIES_PREFIX}-demo-subnet"
SUBNET_CIDR="192.168.0.0/24"
SERVER_NAME="${ENTITIES_PREFIX}-demo-server"
FLAVOR_NAME="standard.small"
IMAGE_NAME="ubuntu-jammy-x86_64"
VM_LOGIN="ubuntu"
ROUTER_NAME="${ENTITIES_PREFIX}-demo-router"
FIP_FILE="fip.txt"
SECGROUP_NAME="${ENTITIES_PREFIX}-demo-secgroup"
SSH_KEYPAIR_DIR="${HOME}/.ssh/generated-keypair"

#############################################################################
# functions
#############################################################################
source ${SCRIPT_DIR}/../../../../common/lib.sh.inc

#############################################################################
# main steps
#############################################################################

# test openstack client version
if ! openstack --version; then
  log "Install openstack client (yum / apt install python3-openstackclient)"
fi

# delete objects (from previous run)
log "Delete previously created objects"
delete_objects_group_project

log "List currently allocated objects"
list_objects

log_keypress "Create (generate) locally SSH keypair, upload public SSH key to cloud"
mkdir -p ${SSH_KEYPAIR_DIR}
chmod 700 ${SSH_KEYPAIR_DIR}
ssh-keygen -t rsa -b 4096 -f "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}"
openstack keypair create --type ssh --public-key "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}.pub" "${KEYPAIR_NAME}"
ls -la ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}*

log_keypress "Create cloud security groups (custom VM firewall) to allow outgoing traffic and incomming SSH traffic on port 22"
openstack security group create --description "${ENTITIES_PREFIX} demo default security group" "${SECGROUP_NAME}"
openstack security group rule create --ingress --proto tcp --remote-ip 0.0.0.0/0 --dst-port 22 "${SECGROUP_NAME}"
openstack security group rule create --egress --proto tcp --remote-ip 0.0.0.0/0 --dst-port 1:65535 "${SECGROUP_NAME}"

log_keypress "Create cloud private network and subnet, so far isolated (CIDR:${SUBNET_CIDR})"
openstack network create "${NETWORK_NAME}"
NETWORK_ID=$(openstack network show "${NETWORK_NAME}" -f value -c id)
openstack subnet create "${SUBNET_NAME}" --network "${NETWORK_ID}" --subnet-range "${SUBNET_CIDR}"

log_keypress "Create cloud VM instance \"${SERVER_NAME}\" with following configuration:\n" \
             "  flavor: ${FLAVOR_NAME}, image/os: ${IMAGE_NAME}, network: ${NETWORK_NAME}\n" \
             "  keypair: ${KEYPAIR_NAME}, sec-group/firewall: ${SECGROUP_NAME})"
openstack server create --flavor "${FLAVOR_NAME}" --image "${IMAGE_NAME}" \
                        --network "${NETWORK_ID}" --key-name "${KEYPAIR_NAME}" \
                        --security-group "${SECGROUP_NAME}" "${SERVER_NAME}"
SERVER_ID=$(openstack server show "${SERVER_NAME}" -f value -c id)

log "Wait for VM instance \"${SERVER_NAME}\" being ACTIVE"
vm_wait_for_status "${SERVER_NAME}" "ACTIVE"

log "Route VM from internal software defined networking outside"
log_keypress "  1] Create route, associate router with external provider network and internal subnet (${SUBNET_CIDR})"
openstack router create "${ROUTER_NAME}"
openstack router set "${ROUTER_NAME}" --external-gateway "${EXTERNAL_NETWORK_NAME}"
openstack router add subnet "${ROUTER_NAME}" "${SUBNET_NAME}"

log_keypress "  2] Allocate single FIP (floating ip) from external provider network"
FIP=$(openstack floating ip create "${EXTERNAL_NETWORK_NAME}" -f value -c name)
echo "${FIP}" > "${FIP_FILE}"
echo "Obtained public FIP ${FIP}"

log_keypress "  3] Assign selected FIP with created VM"
openstack server add floating ip "${SERVER_NAME}" "${FIP}"

log "Test access to the VM server instance"
log_keypress "  1] TCP ping (ncat -z ${FIP} 22)"
test_vm_access "${FIP}"
log_keypress "  2] SSH command (ssh -i ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME} ${VM_LOGIN}@${FIP})"
ssh-keygen -R ${FIP} &>/dev/null
ssh -i "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}" "${VM_LOGIN}@${FIP}" 'echo "";uname -a;uptime'

log_keypress "Object summary:"
list_objects

log_keypress "Teardown of the objects"
delete_objects_group_project

