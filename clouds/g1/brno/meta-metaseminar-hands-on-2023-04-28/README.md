# meta-metaseminar-hands-on-2023-04-28 Infrastructure as Code demo

There are presented two simple approaches:
 * [infrastructure using terraform (best practice)](./terraform_group_project)
 * [infrastructure using command-line openstack client](./commandline)


## Hands-on container

```sh
docker pull registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest
```

## Hands-on Horizon

Using the OpenStack personal project.

### 1. Generate SSH keypair
You may need to have testing SSH key pair
```sh
# docker run -it --rm registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest

# generate in-container ssh keypair
ssh-keygen -t rsa -b 4096
cat ~/.ssh/id_rsa.pub
```

### 2. [Horizon UI login](https://dashboard.cloud.muni.cz)
### 3. Register new SSH pubkey ([Compute -> Key Pairs](https://dashboard.cloud.muni.cz/project/key_pairs))
### 4. Create VM ([Compute -> Instances -> Launch instance](https://dashboard.cloud.muni.cz/project/instances/))
1. Details subpage: Specify Instance name.
1. Source subpage: Select boot source image or existing bootable volume.
1. Flavor subpage: Pick one of available (standard) flavors.
1. Networks subpage: Pick one of the pre-created personal project networks.  147-251-115-pers-proj-net
1. Network ports subpage: skip
1. Security groups subpage: Pick default.
1. Key Pair subpage: Pick created above keypair.
1. Configuration subpage allows to define cloud-init configuration. Skip and Launch instance.

### 5. VM inspection Compute -> Instances -> Pick instance

* Overview
  * Name & ID
  * Spec i.e. flavor
  * Security Groups, verify existing ingress rules
  * Volumes attached
* Interfaces
  * selected network
* (Console) Log
  * inspect cloud-init modifications

### 6. Associating FIP public IPv4 address

### 7. Associating public IPv6 address

### 8. Generating Application Credentials ([Identity -> Application Credentials](https://dashboard.cloud.muni.cz/identity/application_credentials/))


## Hands-on command-line client in group project

```sh
# docker run -it --rm registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest

# read-in the openstack credentials
source /tmp/ac/prod-metaseminar-hands-on-2023-04-28-openrc.sh.inc

# test whether cloud is accessible
openstack version show | grep identity

cd openstack-infrastructure-as-code-automation/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/commandline/
./cmdline-demo-group-project.sh freznicek-demo # use your own name as custom infrastructure prefix in the single hands-on project
```

## Hands-on terraform

```sh
# docker run -it --rm registry.gitlab.ics.muni.cz:443/246254/metaseminar-hands-on-2023-04-28/hands-on-tools:latest

# generate in-container ssh keypair, if not done already
[ -e ~/.ssh/id_rsa.pub ] || ssh-keygen -t rsa -b 4096
cat ~/.ssh/id_rsa.pub
...
# read-in the openstack credentials
source /tmp/ac/prod-metaseminar-hands-on-2023-04-28-openrc.sh.inc

# test whether cloud is accessible
openstack version show | grep identity

# enter terraform workspace
cd openstack-infrastructure-as-code-automation/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/terraform_group_project/

# change infrastructure prefix
mcedit main.tf     # kusername = freznicek

# initial deploy via terraform
terraform init
terraform validate
terraform plan --out plan
terraform apply plan

# login to VM
ncat -z <ip-address> 22
ssh ubuntu@<ip-address>

# doublecheck in horizon in meta-metaseminar-hands-on-2023-04-28 project
# https://dashboard.cloud.muni.cz

# scaling 1->3 VMs
# https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation/-/blob/8c66c1502f2cba26cf9dd51e89c118966ba5e6ed/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/terraform_group_project/main.tf#L25
mcedit main.tf     # nodes_count = 3
terraform validate
terraform plan --out plan
terraform apply plan

# doublecheck in horizon in meta-metaseminar-hands-on-2023-04-28 project
# https://dashboard.cloud.muni.cz

# delete VM via Horizon, and re-deploy via Horizon
# * point out idential internal IP address
terraform validate && terraform plan --out plan && terraform apply plan

# scaling 3->1 VMs
# https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation/-/blob/8c66c1502f2cba26cf9dd51e89c118966ba5e6ed/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/terraform_group_project/main.tf#L25
mcedit main.tf     # nodes_count = 1
terraform validate && terraform plan --out plan && terraform apply plan

# doublecheck in horizon in meta-metaseminar-hands-on-2023-04-28 project
# https://dashboard.cloud.muni.cz

# two disks /dev/sd[ab]
ssh ubuntu@<ip-address> 'lsblk'

# add additional volume (not enough data)
mcedit main.tf     # sdc_volume = 1
terraform validate && terraform plan --out plan && terraform apply plan

# two disks /dev/sd[abc]
ssh ubuntu@<ip-address> 'lsblk'

# remove original volume
mcedit main.tf     # sdb_volume = 0
terraform validate && terraform plan --out plan && terraform apply plan

# two disks /dev/sd[ac]
ssh ubuntu@<ip-address> 'lsblk'

# destroy whole infrastructure
terraform destroy
```
