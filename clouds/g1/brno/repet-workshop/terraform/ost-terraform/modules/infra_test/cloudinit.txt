users:
  - default
  - name: ubuntu
    lock_passwd: false
    shell: /bin/bash
    passwd: '$6$rounds=4096$CVEJGzTk/UGHSJRO$5.gdZHaN58QZke5SKT4O6JgkSmWqlSfUvVNUBuzLt0q3HNKXrTRmSwyM1lh3BlzNiLYkC16QMJDZ83RJHpUCp1'
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC5fFLKPzxna7fq6kh1CHaIQFnpqXybqLqGs4ZpTDbIrEn7xjCsdyxMm9dcptyS0t6BzXO56BlJyYsR1GWo4rp3g8rMmb9u6/oHmMwgn7G/GLgsaAAO5XHW0A3UEJl3JHfCQLHkN1APQ4dy7gNTG24ahH/pcyr4rV0SsjPUCqFqkSMDZxRgfllNGftxWVHR2fYfPALLrGdhR/SjNSIs3pwBIUXaSfF3aBLsjeGBj4y5YsiR9yI3y2gUmpURROofTvtE7Fp8OIgmWCVqRe70CKDbl17HFbz3FIqYwZLAQHILcp1M45zV8koSOjW5+3C/ZJYzBKOnw/a/1Cw3uHFDrZfRqKLMP/gagnoEPRHjfmUsJ3UJO0eXDCXmnH7F48xBI76CgxYl039/SMmJ2mR0KqAHGnwqVmJI3yBGyK+Z4iEwk+JVDLEB14RHiMp2/I/tYpDWFE1IOigFFNLdfaZrVFY1/fD+yGGyFUO1Wo+CKb8tpndLB4H3Yj2MLRDP/aNpLC4M7Aru7hWnUF81aE/VUAqR6CP2vsHzlAOmH08pOlP9FVITinmJqzBL15l+W7q0Rhh4WBRO4ixlrtRJDNL2wm0vf+GiJnXligFtZ7Cw8bk/LcAe37WqcTl0xLKDyPSw4SvWOC2aE6BVuJjPAhoUUcBaNzoBa7lf4eb+FS4tquTZlQ== freznicek@LenovoThinkCentreE73
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILdXyv/LGi5DkOJtPwBqH7EEyXssxgdWqk2CgNx67Clc 506487@mail.muni.cz
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHzqqUEiju2r72oiaQF4zMI/A/vziXSl7IuviEr2z6eh moravcova
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEnjIVyeUk12qFbPPu5KvuFJ4xOVZZAp9W1q4oornrc+ klaris@klaris-tp
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDO9ap8pw3BwGTILl58FipGfDKg9Ao+Milwo/5tNzt08DbxQ9wmL9fP6nwRjJgE8vorVJ+86k+/R3lf3IGcktPgi0n3nYfu8UdVacSSw99Hs/HgfcMEnfwmvelHk1uYGFCNpo98Jrre+nWHFunGYFbTHbdM/bZT8hSB9TaKS4OxedX24aPM2L7GbP83sTXBzoLFNqwkX9s0WnYv2DUnVOKgyoUzJENs1uTfDWNQhYLrivPqrvGEbvYQ0iwaMDSM29m6yJ0I5Ibr5gafz2Cc93wqgQXgBuwZADJ7D0IS7iHinniXMTosbc4ORxhpi8LLWRTJibseOeaP4wucqfAeWkB/yuiWM3BDA5QPGklCXtydlCrropswhfdL66WSvAmVQI7iQbBepg2LPBNhr+0tQeCWfoNXtPBXEm38SG//SzFWYgKIl2eudNLNYTftlbA+++EniiA0YT1kCioW1pd/an5dogPtZimCtPIAwvnC8ukz+M9VTlwPHPG9+OLm2AbwwZjtxqluq8cdcnZN+7os+0TcXdp1hFCxQwhowV6SwyHAW5Y/UBcWpfCjRk5Tfki6RTwMmEujCmD6IzrS3N5xPbAEUB0/qYiSIcFcDejjLF41cD7MSBiowtFCtcDKcIw5iGX441DxCnF87RjAHzczVgt/S5hnHyhm7aYrsF4DY89rfw== radkin@ics.muni.cz
packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - gnupg
  - lsb-release
  - unattended-upgrades
  - python3-pip
  - python3-minimal
runcmd:
  - /opt/script.sh
write_files:
  - path: /etc/docker/daemon.json
    permissions: '0644'
    content: |
      {
        "mtu": 1442
      }
  - path: /opt/script.sh
    permissions: '0755'
    content: |
      #! /bin/bash
      passwd -u ubuntu
      python3 -mpip install openstackclient
      mkdir -p /etc/apt/keyrings
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
      echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
      apt-get update
      apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
      systemctl enable docker
      systemctl start docker
      docker pull urgi/docker_vre_aio
      mkdir /home/ubuntu/work_dir
      wget -P /home/ubuntu/work_dir https://urgi.versailles.inrae.fr/download/repet/banks/REXdb/Viridiplantae_v3.0_ALL_protein-domains_repet_formated.fsa
      source /opt/source.sh
      openstack object save --file /home/ubuntu/work_dir/AthaChr4.fa repet-workshop AthaChr4.fa
      chown -R ubuntu:ubuntu /home/ubuntu/work_dir
  - path: /opt/source.sh
    permissions: '0664'
    content: |
      #!/usr/bin/env bash

      export OS_AUTH_TYPE=v3applicationcredential
      export OS_AUTH_URL=https://identity.cloud.muni.cz/v3
      export OS_IDENTITY_API_VERSION=3
      export OS_REGION_NAME="brno1"
      export OS_INTERFACE=public
      export OS_APPLICATION_CREDENTIAL_ID=xxxxxxxxxxxxxxxxxxxxx
      export OS_APPLICATION_CREDENTIAL_SECRET=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#  - path: /etc/ssh/sshd_config.d/sample.conf
#    content: |
#      PasswordAuthentication yes
#    append: true
ssh_pwauth: true
