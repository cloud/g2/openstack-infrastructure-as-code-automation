variable "vm_name" {
  description = "Name prefix for all resources. Use a-z, 0-9 and the hyphen (-) only."
  default     = "infra-test"
}

variable "public_key" {
  default = "~/.ssh/id_rsa.pub"
}

#########################
# master nodes settings #
#########################
variable "bastion_count" {
  type    = bool
  default = true
}

variable "nodes_a_count" {
  default = 3
}

variable "nodes_b_count" {
  default = 3
}

variable "nodes_name_prefix" {
  description = "Use a-z, 0-9 and the hyphen (-) only."
  default = "master"
}

variable "bastion_flavor" {
  default = "elixir.16core-64ram"
}


variable "nodes_a_flavor" {
  default = "hpc.8core-16ram"
}

variable "nodes_b_flavor" {
  default = "hpc.8core-16ram-ssd-ephem"
}

variable "int_network" {
  description = "Internal network address, use CIDR notation"
  default     = "10.0.0.0/24"
}

variable "pool" {
  description = "FIP pool"
  default     = "public-cesnet-195-113-167-GROUP"
}


variable "image" {
  description = "Image used for both master and worker servers"
  default     = "88f8e72a-bbf0-4ccc-8ff2-4f3188cd0d18"
}

variable "ssh_user_name" {
  default = "ubuntu"
}

variable "volume_size" {
  description = "The size of the volume to create (in gigabytes). "
  default     = "50"
}
