###############################################################
# Define networking                                           #
# Security group rules are in separate file secgroup_rules.tf #
###############################################################

resource "openstack_networking_network_v2" "network_default" {
  name           = "${var.vm_name}_infra_test"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_default" {
  name            = "${var.vm_name}_infra_test"
  network_id      = openstack_networking_network_v2.network_default.id
  cidr            = var.int_network
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "8.8.8.8"]
}

data "openstack_networking_network_v2" "terraform-demo-external-net" {
  name = var.pool
}

resource "openstack_networking_router_v2" "router_default" {
  name                = "${var.vm_name}_infra-test"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.terraform-demo-external-net.id
}

resource "openstack_networking_router_interface_v2" "terraform-demo-router-interface-1" {
  router_id = openstack_networking_router_v2.router_default.id
  subnet_id = openstack_networking_subnet_v2.subnet_default.id
}

# Floating IPs (only for bastion node)
resource "openstack_networking_floatingip_v2" "bastion_fip" {
  pool  = var.pool
}

resource "openstack_compute_floatingip_associate_v2" "bastion_fip_associate" {
  count       = var.bastion_count ? 1 : 0
  floating_ip = openstack_networking_floatingip_v2.bastion_fip.address
  instance_id = openstack_compute_instance_v2.bastion[0].id
}

# Ports
resource "openstack_networking_port_v2" "bastion_ports" {
  count              = var.bastion_count ? 1 : 0
  name               = "${var.vm_name}_bastion_port_${count.index+1}"
  network_id         = openstack_networking_network_v2.network_default.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default.id
  }
}

resource "openstack_networking_port_v2" "nodes_a_ports" {
  count              = var.nodes_a_count
  name               = "${var.vm_name}_nodes_a_port_${count.index+1}"
  network_id         = openstack_networking_network_v2.network_default.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default.id
  }
}

resource "openstack_networking_port_v2" "nodes_b_ports" {
  count              = var.nodes_b_count
  name               = "${var.vm_name}_nodes_b_port_${count.index+1}"
  network_id         = openstack_networking_network_v2.network_default.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = openstack_networking_subnet_v2.subnet_default.id
  }
}

