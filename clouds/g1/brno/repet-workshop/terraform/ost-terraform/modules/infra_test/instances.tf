####################
# Define instances #
####################
resource "openstack_compute_instance_v2" "bastion" {
  count           = var.bastion_count ? 1 : 0
  name            = "${var.vm_name}-bastion"
  image_id        = var.image
  flavor_name     = var.bastion_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.vm_name}-bastion.local\n${file("${path.module}/cloudinit-bastion.txt")}"

  network {
    uuid = openstack_networking_network_v2.network_default.id
    port = element(openstack_networking_port_v2.bastion_ports.*.id, count.index)
  }
}

resource "openstack_compute_instance_v2" "nodes_a" {
  count           = var.nodes_a_count
  name            = "${var.vm_name}-a-${count.index+1}"
  image_id        = var.image
  flavor_name     = var.nodes_a_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.vm_name}-${count.index+1}.local\n${file("${path.module}/cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.network_default.id
    port = element(openstack_networking_port_v2.nodes_a_ports.*.id, count.index)
  }

  block_device {
    uuid                  = var.image
    source_type           = "image"
    volume_size           = var.volume_size
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }
}

resource "openstack_compute_instance_v2" "nodes_b" {
  count           = var.nodes_b_count
  name            = "${var.vm_name}-b-${count.index+1}"
  image_id        = var.image
  flavor_name     = var.nodes_b_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.vm_name}-${count.index+1}.local\n${file("${path.module}/cloudinit.txt")}"

  network {
    uuid = openstack_networking_network_v2.network_default.id
    port = element(openstack_networking_port_v2.nodes_b_ports.*.id, count.index)
  }

  block_device {
    uuid                  = var.image
    source_type           = "image"
    volume_size           = var.volume_size
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }
}
