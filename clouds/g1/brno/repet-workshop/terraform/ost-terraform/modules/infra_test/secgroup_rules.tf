##################################
# Define Network Security Groups #
##################################


resource "openstack_networking_secgroup_v2" "secgroup_default" {
  name        = "${var.vm_name}_infra-test"
  description = "Security group for Kubernetes demo"
}


# Allow all internal TCP & UDP

/* resource "openstack_networking_secgroup_rule_v2" "alltcp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = var.int_network
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

resource "openstack_networking_secgroup_rule_v2" "alludp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = var.int_network
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
} */


# External communication
# HTTP(S)

resource "openstack_networking_secgroup_rule_v2" "https4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

resource "openstack_networking_secgroup_rule_v2" "http4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}



# ICMP

resource "openstack_networking_secgroup_rule_v2" "icmp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}



# SSH

resource "openstack_networking_secgroup_rule_v2" "ssh4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

# LB

/* resource "openstack_networking_secgroup_rule_v2" "lb4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6443
  port_range_max    = 6443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
} */