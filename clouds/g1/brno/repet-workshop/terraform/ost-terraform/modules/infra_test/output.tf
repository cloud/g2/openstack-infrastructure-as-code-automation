output "bastion_instance_ip" {
  value = openstack_compute_instance_v2.bastion[*].access_ip_v4
}

output "bastion_floating_ip" {
  value = openstack_compute_floatingip_associate_v2.bastion_fip_associate[*].floating_ip
}

output "nodes_a_instance_ip" {
  value = openstack_compute_instance_v2.nodes_a[*].access_ip_v4
}

output "nodes_a_name" {
  value = openstack_compute_instance_v2.nodes_a[*].name
}

output "nodes_b_instance_ip" {
  value = openstack_compute_instance_v2.nodes_b[*].access_ip_v4
}

output "nodes_b_name" {
  value = openstack_compute_instance_v2.nodes_b[*].name
}