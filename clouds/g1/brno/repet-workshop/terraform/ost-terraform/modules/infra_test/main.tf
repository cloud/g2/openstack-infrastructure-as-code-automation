
resource "openstack_compute_keypair_v2" "pubkey" {
  name       = "${var.vm_name}-infra-test"
  public_key = file("${var.public_key}")
}

