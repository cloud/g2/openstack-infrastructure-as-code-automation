# Terraform Kubernetes Training

This Terraform module creates up to 2 kind of VMs (each with different flavor) + 1 bastion with floating IP.  

Cloud-init add following:
 - Add ssh keys and password settings for ubuntu user
 - Install docker with the correct MTU and pull the image
 - Install openstack-cli, source OpenStack application credentials, and download the file from swift  

## Create Infrastructure

1. Clone the repository.
1. Load you OpenStack application credentials to environment variables `source ~/conf/prod-meta-cloud-new-openstack-all-roles-openrc.sh`
1. Override any variable if needed. Every variable specified in [modules/kube_training/variables.tf](modules/kube_training/variables.tf) can be overridden in the [main.tf](main.tf) file in its *module* section.
1. If create infrastructure for kubespray see [this section](#kubespray)
1. In the root folder run `terraform init`.
1. In the root folder run `terraform validate`.
1. Run `terraform plan -out plan1` to generate terraform plan.
1. Run `terraform apply "plan1"` to apply the plan.

## Kubespray

If you want to access kube-api via HA floating IP you need create port with attached floating IP after installation kubernetes via kubespray.

First, you have to apply infrastructure with `kube_fip = true` and `kube_fip_create_port = false` and after kubespray is installed, change to `kube_fip_create_port = true` and `terraform apply` again. Also, you can set `kube_vip` which has to be a free IP address in the given subnet.

## Destroy Infrastructure

To delete all created resources run the following commands:

```
terraform plan -destroy -out plan1
terraform apply "plan1"
``` 

## SSH to Workers

To connect to worker machines just use `sshuttle`.

```
sshuttle -r debian@<any-master-ip> 10.0.0.0/24 -x 147.251.62.9/32
```
