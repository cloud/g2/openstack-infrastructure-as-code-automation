output "b_infra_test_instance_ip_bastion" {
  value = module.infra_test.bastion_instance_ip
}

output "a_infra_test_instance_floating_ip_bastion" {
  value = module.infra_test.bastion_floating_ip
}

output "d_infra_test_instance_ip_a" {
  value = module.infra_test.nodes_a_instance_ip
}

output "c_infra_test_instance_name_a" {
  value = module.infra_test.nodes_a_name
}

output "e_infra_test_instance_name_b" {
  value = module.infra_test.nodes_b_name
}

output "f_infra_test_instance_ip_b" {
  value = module.infra_test.nodes_b_instance_ip
}


