terraform {
  backend "local" {}
}

terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
  }
}

module "infra_test" {
  source = "./../modules/infra_test"

  public_key = "~/.ssh/klaris-tp.pub"

  vm_name        = "repet-workshop"
  nodes_a_count  = 29
  nodes_a_flavor = "hpc.8core-16ram"
  nodes_b_count  = 18
  nodes_b_flavor = "elixir.8core-16ram"
  volume_size    = 100

}
