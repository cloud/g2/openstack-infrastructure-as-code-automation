#!/bin/bash
set -e

### USAGE: run_vm_with_attached_volume.sh <VOLUME_TO_ATTACH_ID> <SSH_PUB_KEY_LOCATION>

THIS_SCRIPT_DIR=$(dirname "$(realpath -s "$0")")

VOLUME_TO_ATTACH_ID="$1"
SSH_PUB_KEY_LOCATION="$2"

echo "Running Terraform init"
terraform -chdir=${THIS_SCRIPT_DIR}/terraform init

echo "Running Terraform plan (volume id: $VOLUME_TO_ATTACH_ID, public key path: $SSH_PUB_KEY_LOCATION)"
terraform -chdir=${THIS_SCRIPT_DIR}/terraform plan -out create.tfplan \
  -var "volume_id_to_attach=$VOLUME_TO_ATTACH_ID" \
  -var "ssh_public_key_location=$SSH_PUB_KEY_LOCATION"
printf "\n\n*********************************************************************************************************\n"

read -p "PRESS ENTER once you are OK with whatever Terraform planned above ...: "
terraform -chdir=${THIS_SCRIPT_DIR}/terraform apply create.tfplan

printf "\n\n*********************************************************************************************************\n"
echo "New VM was created with the requested volume ($VOLUME_TO_ATTACH_ID) attached"
echo "VM name is: $(terraform -chdir=terraform output vm_name)"
