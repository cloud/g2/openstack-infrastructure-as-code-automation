#!/bin/bash
set -eo pipefail

###
### This script acquires snapshot of given volume (VOLUME_ID) and initiates transfer so users of a project who get the
### transfer details (transfer id, transfer secret) can accept the transfer and place the snapshot into their project.
### Volume transfer details are written in json file and will be used at the moment of snapshot transfer acceptance.
###
### Usage: acquire_snapshot_and_create_volume_transfer.sh <VOLUME_ID>
###
### Links: https://openmetal.io/docs/manuals/users-manual/managing-backups-in-openstack
###
###

function check_and_wait_until_available() {
  local OPERATION_NAME="$1"
  local CHECK_COMMAND="$2"

  while true; do
    STATE=$(eval $CHECK_COMMAND)
    if [ "$STATE" != "available" ]; then
      echo "$OPERATION_NAME in progress"
      sleep 10
      continue
    else
      echo "$OPERATION_NAME completed"
      break;
    fi
  done
}

VOLUME_ID="$1"
THIS_SCRIPT_DIR=$(dirname "$(realpath -s "$0")")

TIMESTAMP=$(date +%s)
SNAPSHOT_NAME="snapshot_${TIMESTAMP}"

# let fail the script at this moment in case no app credentials are sourced or the subjected volume does not exist
openstack volume show $VOLUME_ID > /dev/null

echo "Starting snapshot creation of volume $VOLUME_ID"
SNAPSHOT_ID=$(openstack volume snapshot create \
    --volume ${VOLUME_ID} \
    --force \
    -f json \
    ${SNAPSHOT_NAME} | jq -r ".id")

check_and_wait_until_available "Snapshot creation ($SNAPSHOT_ID)" "openstack volume snapshot show $SNAPSHOT_ID -f value -c status"

echo "Starting volume creation based on snapshot $SNAPSHOT_ID"
VOLUME_ID=$(openstack volume create \
    --snapshot ${SNAPSHOT_ID} \
    -f json \
    ${SNAPSHOT_NAME} | jq -r ".id")

check_and_wait_until_available "Volume creation ($VOLUME_ID)" "openstack volume show ${VOLUME_ID} -f value -c status"

echo "Creating volume transfer request for volume creation of volume $VOLUME_ID"
VOLUME_TRANSFER_DETAILS=$(openstack volume transfer request create \
    --name ${SNAPSHOT_NAME}_transfer \
    -f json \
    ${VOLUME_ID})

VOLUME_TRANSFER_ID=$(echo "${VOLUME_TRANSFER_DETAILS}" | jq -r '.id')
VOLUME_TRANSFER_DETAILS_LOCATION=${THIS_SCRIPT_DIR}/volume-transfer_${VOLUME_TRANSFER_ID}.json
echo ${VOLUME_TRANSFER_DETAILS} > ${VOLUME_TRANSFER_DETAILS_LOCATION}

echo "Volume transfer has been initiated. Details needed to complete the transfer are found in $VOLUME_TRANSFER_DETAILS_LOCATION"
