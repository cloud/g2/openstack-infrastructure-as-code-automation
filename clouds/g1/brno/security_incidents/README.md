# Security incidents in e-INFRA / MetaCentrum Cloud
This document describes details of process when CyberSecurity incidents is detected in the MetaCentrum Cloud.

## Workflow

![metacentrum_cloud_incidents.drawio.png](pictures/metacentrum_cloud_incidents.drawio.png)


The MetaCentrum Security team detects suspicious VM and creates ticket in the RT instance `rt.cesnet.cz`, queue `security` with cloud-team [core members](https://gitlab.ics.muni.cz/cloud/knowledgebase/-/blob/master/team/onboarding/organizational-topics.md?ref_type=heads#meet-the-team) in CC and specifies actions to be taken by the MetaCentrum Cloud team including:
- Provide identity of the VM owner. Project detail including identity of the owner is available in [JSON format](https://gitlab.ics.muni.cz/cloud/ip-owners/-/blob/master/ip_project_owners.json) and produced daily (always check [tags](https://gitlab.ics.muni.cz/cloud/ip-owners/-/tags)).
- Share snapshot of the VM.
- Provide instruction how to treat the VM further (stop, keep, etc).
    - In case of stopping, include locking the VM: `SERVER_ID=... openstack server stop $SERVER_ID && openstack server lock $SERVER_ID`.

MetaCentrum Security handles communication with the user - owner of the Openstack project where the VM originates from.

Cloud team transfers the VM snapshot into Openstack project `meta-cloud-metac_sec-cerit_sec`. Access to this project is granted via Perun groups `meta-cloud-admins` (Cloud team) and `meta-sec` (Security team). For the automation checkout [scripts residing in GitLab](https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation/-/tree/master/clouds/g1/brno/security_incidents):

- [`./identify_cloud_resources.sh --fip <public-fip-address>`](./identify_cloud_resources.sh) # reports associated volumes and more info
- [`./acquire_snapshot_and_create_volume_transfer.sh <THE_ORIGINAL_VM_VOLUME_ID>`](./acquire_snapshot_and_create_volume_transfer.sh)
- [`./accept_volume_transfer.sh <VOLUME_TRANSFER_ID>`](./accept_volume_transfer.sh) => results in a new volume with id shared with the Security team.

MetaCentrum Security runs a new VM with the new volume attached (not mounted). The VM can be accessed by a private key which is complementary to the provided public key passed in.
- [`./run_vm_with_attached_volume.sh <VOLUME_ID> <SSH_PUB_KEY_LOCATION>`](./run_vm_with_attached_volume.sh)

MetaCentrum Security cleans up the resources
- [`./destroy_the_vm.sh`](./destroy_the_vm.sh)


#### Links and metrics
 - absolute [CPU usage](https://thanos-query1.cloud.muni.cz/graph?g0.expr=libvirt_domain_info_cpu_time_seconds_total&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) is calculated as `sum(increase(libvirt_domain_info_cpu_time_seconds_total ...))` per given time T. 
   - relative usage is then calculated as `increase(libvirt_domain_info_cpu_time_seconds_total{domain="..."}[7d])`. To normalize it regarding the CPU cores, divide it by number of cores, i.e. `increase(libvirt_domain_info_cpu_time_seconds_total{domain="..."}[7d])/(libvirt_domain_info_virtual_cpus{domain="..."}*T)*100`




#### The provided automation requires following to be in place
| Script                                           | Requires                                                                                                                               |
|--------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| [`identify_cloud_resources.sh`](./identify_cloud_resources.sh) | bash, [openstack-cli](https://docs.openstack.org/ocata/user-guide/cli.html), jq, Application credentials (`admin`)                         |
| [`acquire_snapshot_and_create_volume_transfer.sh`](./acquire_snapshot_and_create_volume_transfer.sh) | bash, [openstack-cli](https://docs.openstack.org/ocata/user-guide/cli.html), jq, Application credentials (`admin`)                         |
| [`accept_volume_transfer.sh`](./accept_volume_transfer.sh)                      | bash, [openstack-cli](https://docs.openstack.org/ocata/user-guide/cli.html), jq, Application credentials (`meta-cloud-metac_sec-cerit_sec`)|
| [`run_vm_with_attached_volume.sh`](./run_vm_with_attached_volume.sh)                 | bash, [terraform](https://www.terraform.io), Application credentials (`meta-cloud-metac_sec-cerit_sec`)                                |      
| [`destroy_the_vm.sh`](./destroy_the_vm.sh)                              | bash, [terraform](https://www.terraform.io), Application credentials (`meta-cloud-metac_sec-cerit_sec`)                                |
