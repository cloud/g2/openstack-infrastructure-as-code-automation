####################
# Define instances #
####################

data "openstack_images_image_v2" "nodes_image" {
  name        = var.nodes_image
}

locals {
  vm_name = format("%s-%s__%s", var.infra_name, var.nodes_name, formatdate("YYYYMMDDhhmm", timestamp()))
}

output "vm_name" {
  value = local.vm_name
}


resource "openstack_compute_instance_v2" "nodes" {
  count           = 1
  name            = local.vm_name
  image_name      = var.nodes_image
  flavor_name     = var.nodes_flavor
  key_pair        = openstack_compute_keypair_v2.ssh_key_pair.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]

  network {
    uuid = var.internal_network_creation_enable ? openstack_networking_network_v2.network_default[0].id : data.openstack_networking_network_v2.internal_shared_personal_network[0].id
    port = element(openstack_networking_port_v2.nodes_ports.*.id, count.index)
  }

  block_device {
    uuid                  = data.openstack_images_image_v2.nodes_image.id
    source_type           = "image"
    volume_size           = var.nodes_volume_size
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }

  block_device {
    uuid                  = "${var.volume_id_to_attach}"
    source_type           = "volume"
    destination_type      = "volume"
    boot_index            = -1
  }
}

output "server_instance_id" {
  value = openstack_compute_instance_v2.nodes[0].id
}
