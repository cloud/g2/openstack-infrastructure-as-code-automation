
resource "openstack_compute_keypair_v2" "ssh_key_pair" {
  name       = "${var.infra_name}-keypair"
  public_key = file(var.ssh_public_key_location)
}
