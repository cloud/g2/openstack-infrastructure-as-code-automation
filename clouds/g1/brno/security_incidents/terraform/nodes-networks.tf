# Floating IPs
resource "openstack_networking_floatingip_v2" "nodes_fips" {
  count    = 1
  pool     = var.public_external_network
}

resource "openstack_compute_floatingip_associate_v2" "nodes_fips_associations" {
  count       = 1
  floating_ip = element(openstack_networking_floatingip_v2.nodes_fips.*.address, count.index)
  instance_id = element(openstack_compute_instance_v2.nodes.*.id, count.index)
}
