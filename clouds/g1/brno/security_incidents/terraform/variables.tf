#########################################################
# general configuration (defaults on G1 production cloud)
#########################################################


variable "infra_name" {
  description = "Infrastructure (profile) name. Used as a name prefix. Must match [a-zA-Z0-9-]+ regexp."
  default     = "meta-cloud-security-artifact-share"
}

variable "internal_network_cidr" {
  description = "Internal network address, use CIDR notation"
  default     = "10.10.10.0/24"
}

variable "public_external_network" {
  description = "Cloud public external network pool"
  default     = "public-cesnet-195-113-167-GROUP"
}

variable "router_creation_enable" {
  description = "Create dedicated router instance. true/false ~ create new / reuse existing personal router"
  default     = true
}

variable "internal_network_creation_enable" {
  description = "Create dedicated internal network. true/false ~ create new / reuse existing personal network"
  default     = true
}

variable "internal_network_name" {
  description = "Internal network name. Either dedicated new network or existing personal network name"
  default     = "<var.infra_name>_network"
}

variable "internal_subnet_creation_enable" {
  description = "Create dedicated subnet instance. true/false ~ create new / reuse existing personal subnet"
  default     = true
}

variable "internal_subnet_name" {
  description = "Internal network subnet name. Either dedicated new subnet or existing personal subnet name"
  default     = "<var.infra_name>_subnet"
}

variable "nodes_name" {
  description = "Name of the nodes. Must match [a-zA-Z0-9-]+ regexp."
  default = "nodes"
}

variable "nodes_flavor" {
  default = "standard.large"
}

variable "nodes_image" {
  description = "nodes OS: Image name"
  default     = "ubuntu-jammy-x86_64"
}

variable "nodes_ssh_user_name" {
  default = "ubuntu"
}

variable "nodes_volume_size" {
  description = "The size of the volume to create (in gigabytes) for root filesystem. "
  default     = "10"
}

variable "ssh_public_key_location" {
  description = "Provide location of public key for which its complementary private key shall allow ssh access"
  type = string
}

variable "volume_id_to_attach" {
  description = "Provide volume ID"
  type = string
}
