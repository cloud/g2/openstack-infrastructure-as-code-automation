#!/bin/bash
set -e

### USAGE: accept_volume_transfer.sh <LOCATION_OF_VOLUME_TRANSFER_DESCRIPTOR>

THIS_SCRIPT_LOCATION=$(dirname "$(realpath -s "$0")")

read -p "Ensure that Application Credentials for target Openstack project have been sourced. Press ENTER to continue."

VOLUME_TRANSFER_DETAILS=$(cat "$1" | jq -r '.id + "," + .auth_key')
VOLUME_TRANSFER_ID=$(echo $VOLUME_TRANSFER_DETAILS | awk -F ',' '{print $1}')
VOLUME_TRANSFER_SECRET=$(echo $VOLUME_TRANSFER_DETAILS | awk -F ',' '{print $2}')

VOLUME_ID=$(openstack volume transfer request accept \
  --auth-key ${VOLUME_TRANSFER_SECRET} \
  ${VOLUME_TRANSFER_ID} -c volume_id -f value)

echo "Transfer ${VOLUME_TRANSFER_ID} completed. The volume id ${VOLUME_ID} can be attached to a VM"
