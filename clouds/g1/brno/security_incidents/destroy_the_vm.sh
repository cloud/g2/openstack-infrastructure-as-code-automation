#!/bin/bash
set -e
THIS_SCRIPT_LOCATION=$(dirname "$(realpath -s "$0")")
TEMP_FILE_LOCATION=${THIS_SCRIPT_LOCATION}/whatever_delme

terraform -chdir=${THIS_SCRIPT_LOCATION}/terraform init
echo "Running Terraform plan to destroy"

terraform -chdir=${THIS_SCRIPT_LOCATION}/terraform plan -destroy -out destroy.tfplan \
  -var "volume_id_to_attach=" \
  -var "ssh_public_key_location=$(mktemp)"
printf "\n\n*********************************************************************************************************\n"
read -p "PRESS ENTER once you are OK with whatever Terraform planned above ...:"

terraform -chdir=${THIS_SCRIPT_LOCATION}/terraform apply destroy.tfplan
