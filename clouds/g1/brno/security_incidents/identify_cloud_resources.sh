#!/bin/bash
#
# identify_cloud_resources.sh -h|--help
#   dumps this help
#
# identify_cloud_resources.sh -i|--fip <ip-address>
#   identifies cloud resources from the associated floating IP address
# identify_cloud_resources.sh -s|--server <server-name-or-id>
#   identifies cloud resources from the openstack server name/ID
#
# This script identifies what cloud resources are necessary to dump
#
# Example executions:
# Find information about VM using FIP 147.251.124.228
# * SSH_ARGS="-i $HOME/.ssh/id_rsa.LenovoThinkCentreE73" ./identify_cloud_resources.sh --fip 147.251.124.228
#

set -eo pipefail

SSH_ARGS=${SSH_ARGS:-""}

function usage() {
    local file="$1"
    awk '{if(NR>1){if($1 ~ /^#(.+|$)/){print substr($0,3)}else{exit(0)}}}' $file
}

PARGS=$(getopt -o "h,s:,i:" -l "help,server:,fip:" -n "$0" -- "$@")
eval set -- "$PARGS"
HELP=0
SERVER=""
FIP=""
while [ "$1" != "--" ] ; do
    case "$1" in
      -h|--help)
          usage "$0"
          exit 0
          ;;
      -i|--fip)
          FIP="$2"
          shift 2
          ;;
      -s|--server)
          SERVER="$2"
          shift 2
          ;;
    esac
done

if [ -z "${FIP}" -a -z "${SERVER}" ]; then
    usage "$0"
    exit 1
fi

echo "Connecting to OpenStack cloud:"
openstack version show -fcsv | grep identity

if [ -n "${FIP}" ]; then
  echo "Translate FIP to VM:"
  fip_json="$(openstack floating ip show "${FIP}" -fjson)"
  port_id=$(echo "${fip_json}" | jq -r '.port_id')
  echo "  * port ID: ${port_id}"
  port_json="$(openstack port show "${port_id}" -fjson)"
  SERVER=$(echo "${port_json}" | jq -r '.device_id')
  echo "  * server ID: ${SERVER}"
fi

server_json="$(openstack server show "${SERVER}" -fjson)"

echo "OpenStack server details:"
server_id="$(echo "${server_json}" | jq -r '.id')"
echo "* ID: ${server_id}"
echo "* name: $(echo "${server_json}" | jq -r '.name')"
echo "* project:"
project_id="$(echo "${server_json}" | jq -r '.project_id')"
echo "* * ID: ${project_id}"
project_json="$(openstack project show "${project_id}" -fjson)"
echo "* * name: $(echo "${project_json}" | jq -r '.name')"
echo "* volumes: $(echo "${server_json}" | jq -rc '.volumes_attached')"
hypervisor_hostname="$(echo "${server_json}" | jq -rc '."OS-EXT-SRV-ATTR:host"')"
echo "* hypervisor: ${hypervisor_hostname}"
libvirt_domain_name=$(echo "${server_json}" | jq -rc '."OS-EXT-SRV-ATTR:instance_name"')
echo "* libvirt-domain-name: $libvirt_domain_name"
echo "* security-groups: $(echo "${server_json}" | jq -rc '.security_groups')"
security_group_names=$(echo "${server_json}" | jq -r '.security_groups[] | .name')
security_groups_json=$(openstack security group list --project ${project_id} -fjson)
for i_sg_name in ${security_group_names}; do
    i_sg_id="$(echo "${security_groups_json}" | jq -r ".[] | select(.Name == \"${i_sg_name}\") | .ID")"
    echo "* *  security-group ${i_sg_name} (ID: ${i_sg_id})"
    openstack security group show ${i_sg_id} -fyaml | grep -Ev 'null|tags:|tenant_id:|revision_number:|security_group_id:|  id:|project_id:'
done

if ssh ${SSH_ARGS} "root@${hypervisor_hostname}" "uname -a" >/dev/null; then
    echo "List libvirt domain block devices:"
    ssh ${SSH_ARGS} "root@${hypervisor_hostname}" "virsh domblklist ${server_id}"
else
    echo "Libvirt domain block device list is not available."
fi


VM_CPU_USAGE_THANOS_URL='https://thanos-query1.cloud.muni.cz/graph?g0.expr=sum(increase(libvirt_domain_info_cpu_time_seconds_total%7Bhostname%3D%22__HYPERVISOR_NAME__%22%2C%20domain%3D%22__LIBVIRT_DOMAIN__%22%7D%5B360s%5D))&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D'
VM_CPU_USAGE_THANOS_URL=$(echo "$VM_CPU_USAGE_THANOS_URL" | sed s/__HYPERVISOR_NAME__/$hypervisor_hostname/)
VM_CPU_USAGE_THANOS_URL=$(echo "$VM_CPU_USAGE_THANOS_URL" | sed s/__LIBVIRT_DOMAIN__/$libvirt_domain_name/)
echo "* Link to Thanos metric - CPU usage:  $VM_CPU_USAGE_THANOS_URL"
