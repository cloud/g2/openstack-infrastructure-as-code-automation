# extra variabes for an OpenStack personal project
router_creation_enable           = false
internal_network_creation_enable = false
internal_network_name            = "147-251-115-pers-proj-net"
internal_subnet_creation_enable  = false
internal_subnet_name             = "subnet-147-251-115-pers-proj-net"
public_external_network          = "public-muni-147-251-115-PERSONAL"
nodes_flavor                     = "standard.small"
bastion_flavor                   = "standard.small"
