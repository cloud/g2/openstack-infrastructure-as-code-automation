# extra variabes for an OpenStack personal project
router_creation_enable           = false
internal_network_creation_enable = false
internal_network_name            = "internal-ipv4-general-private"
internal_subnet_creation_enable  = false
internal_subnet_name             = "internal-ipv4-general-private-172-16-0-0"
public_external_network          = "external-ipv4-general-public"
nodes_flavor                     = "e1.small"
bastion_flavor                   = "e1.small"
