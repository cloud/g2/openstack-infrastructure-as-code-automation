# Terraform demonstration

This Terraform module is able to demonstrate creation of two most used cloud infrastructure patterns:

## Two tier infrastructure: public bastion and private VM farm

Infrastructure consist of:
 - public facing small bastion VM (sometimes called as jump VM)
 - private VM farm

![two-tier-infra.png](/clouds/common/pictures/two-tier-infra.png)

## Single tier infrastructure i.e. public facing VM farm

Infrastructure consist of:
 - public facing VM farm

![single-tier-infra.png](/clouds/common/pictures/single-tier-infra.png)


## Cloud VM configuration during system boot using cloud-init
Terraform demonstrates [how to configure VM servers on creation with cloud-init](modules/common/nodes-cloudinit.txt):
 - Add ssh keys, disable SSH password auth
 - Create partition and filesystemand mount extra data from extra volume

## Handling different project restrictions (quotas, shared networks, ...)

e-INFRA.CZ OpenStack cloud distinguishes between two project types: personal and group
([more the topic](https://docs.e-infra.cz/compute/openstack/technical-reference/brno-site/get-access/#personal-project)).

Terraform demo code comes with terraform variable files (for instance [`personal-projects.tfvars`](./personal-projects.tfvars)) which show what has to be tuned for particular cloud and project type.


## Using the terraform demo

1. Clone the repository.
1. Load you OpenStack application credentials to environment variables `source project_openrc.sh.inc`
1. Override any infrastructure variables in [main.tf](main.tf) file if needed. Full set of variables can be found in [modules/common/variables.tf](/clouds/common/general/terraform/modules/common/variables.tf).
1. In the [terraform root directory](/clouds/g2/ostrava/general/terraform) run following command `./terraform-demo.sh <cloud-environment-name>` (for instance `g2-prod-brno`)

Detailed terminal transcripts can be found in [logs/](./logs/) subdirectory..


## Access to the VM nodes

In single tier infrastructure you access directly the individual VM nodes via SSH on public IP addresses.
Two tier infrastructure requires the access following way:
1. Establish the connection with bastion
```sh
sshuttle -r ubuntu@<bastion-ip>
```
1. Connect directly to VM nodes via SSH on private IP addresses:
```sh
ssh  ubuntu@<vm-node-ip-from-10.10.10.0/24>
```
