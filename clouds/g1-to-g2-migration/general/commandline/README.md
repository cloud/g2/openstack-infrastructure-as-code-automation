# Build OpenStack infrastructure from command-line using openstack client

## Pre-requisites
 * Linux/Mac/WSL2 terminal
 * BASH shell
 * installed openstack client ([how?](https://docs.fuga.cloud/how-to-use-the-openstack-cli-tools-on-linux))
 * MetaCentrum OpenStack cloud [group project granted](https://docs.e-infra.cz/compute/openstack/technical-reference/brno-site/get-access/#group-project).
 * downloaded application credentials from OpenStack Horizon dashboard ([how?](https://docs.cloud.muni.cz/cloud/cli/#getting-credentials)) and store as text file `project_openrc.sh.inc`.

## How to use the script
```sh
# in bash shell
source project_openrc.sh.inc
./cmdline-demo.sh g2-prod-brno basic-infrastructure-1
```
See [linked reference executions](./logs).

## Infrastructure overview
How does the basic infrastructure looks like?
* single VM (ubuntu-jammy)
  * VM firewall opening port 22
  * VM SSH keypair generated locally and pubkey uploaded to cloud
  * attached additional volume (size 10GB)
* private subnet and network
* router to external internet
* public floating ip address

![basic-infrastructure.png](/clouds/common/pictures/basic-infrastructure.png)

## G1 and G2 cloud differences

Inspect ([g1-prod-brno.sh.inc](./g1-prod-brno.sh.inc), [g2-prod-brno.sh.inc](./g2-prod-brno.sh.inc), [g2-prod-ostrava.sh.inc](./g2-prod-ostrava.sh.inc)) include shell files to see what needs to be changed.