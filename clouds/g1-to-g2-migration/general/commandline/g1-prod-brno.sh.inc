
CLOUD_ENV_NAME="g1-prod-brno"

declare -A ROUTER_NAME_ARR
ROUTER_NAME_ARR[personal]="${ROUTER_NAME:-"router-147-251-115-pers-proj-net"}" # re-use existing entity
ROUTER_NAME_ARR[group]="${ROUTER_NAME:-"${ENTITIES_PREFIX}-demo-router"}"
declare -A NETWORK_NAME_ARR
NETWORK_NAME_ARR[personal]="${NETWORK_NAME:-"147-251-115-pers-proj-net"}"      # re-use existing entity
NETWORK_NAME_ARR[group]="${NETWORK_NAME:-"${ENTITIES_PREFIX}-demo-network"}"
declare -A SUBNET_NAME_ARR
SUBNET_NAME_ARR[personal]="${SUBNET_NAME:-"subnet-147-251-115-pers-proj-net"}" # re-use existing entity
SUBNET_NAME_ARR[group]="${SUBNET_NAME:-"${ENTITIES_PREFIX}-demo-subnet"}"
declare -A EXTERNAL_NETWORK_NAME_ARR
EXTERNAL_NETWORK_NAME_ARR[personal]="public-muni-147-251-115-PERSONAL"         # re-use existing entity
EXTERNAL_NETWORK_NAME_ARR[group]="public-muni-147-251-124-GROUP"               # re-use existing entity

FLAVOR_NAME="${FLAVOR_NAME:-"standard.medium"}"
