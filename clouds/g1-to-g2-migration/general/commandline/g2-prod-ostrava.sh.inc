
CLOUD_ENV_NAME="g2-prod-ostrava"

declare -A ROUTER_NAME_ARR
ROUTER_NAME_ARR[personal]="${ROUTER_NAME:-"internal-ipv4-general-private"}"            # re-use existing entity
ROUTER_NAME_ARR[group]="${ROUTER_NAME:-"${ENTITIES_PREFIX}-demo-router"}"
declare -A NETWORK_NAME_ARR
NETWORK_NAME_ARR[personal]="${NETWORK_NAME:-"internal-ipv4-general-private"}"          # re-use existing entity
NETWORK_NAME_ARR[group]="${NETWORK_NAME:-"${ENTITIES_PREFIX}-demo-network"}"
declare -A SUBNET_NAME_ARR
SUBNET_NAME_ARR[personal]="${SUBNET_NAME:-"internal-ipv4-general-private-172-22-0-0"}" # re-use existing entity
SUBNET_NAME_ARR[group]="${SUBNET_NAME:-"${ENTITIES_PREFIX}-demo-subnet"}"
declare -A EXTERNAL_NETWORK_NAME_ARR
EXTERNAL_NETWORK_NAME_ARR[personal]="${EXTERNAL_NETWORK_NAME}"                         # re-use existing entity
EXTERNAL_NETWORK_NAME_ARR[group]="${EXTERNAL_NETWORK_NAME}"                            # re-use existing entity

FLAVOR_NAME="${FLAVOR_NAME:-"e1.medium"}"
