# general IaaS infrastructure demo

Assuming you are added ato a group project and you can log in via [e-INFRA CZ OpenStack cloud dashboard](https://horizon.brno.openstack.cloud.e-infra.cz/) using one of supported federations (e-INFRA CZ, ...).

We recommend to build custom cloud infrastructure with Terraform or openstack client rather than using [e-INFRA CZ OpenStack cloud dashboard](https://horizon.brno.openstack.cloud.e-infra.cz/).

Below demos show in detail how to do so.

## [Terraform `general` demo](./terraform)

Terraform demo shows how to automate building highly scalable IaaS infrastructure.

## [OpenStack client `general` demo](./commandline)

OpenStack shell script demo shows how to automate small IaaS infrastructure which does not need additional scalability.
