# Build OpenStack infrastructure from command-line using openstack client

## Pre-requisites
 * Linux/Mac/WSL2 terminal
 * BASH shell
 * installed openstack client ([how?](https://docs.fuga.cloud/how-to-use-the-openstack-cli-tools-on-linux))
 * e-INFRA OpenStack cloud personal/group project granted.
 * downloaded application credentials from OpenStack Horizon dashboard ([how?](https://docs.e-infra.cz/compute/openstack/how-to-guides/obtaining-api-key/)) and store as text file `project_openrc.sh.inc`.

## How to use the script
```sh
# in bash shell
source project_openrc.sh.inc
./cmdline-demo.sh basic-infrastructure-1
```
See linked reference executions for [personal](./cmdline-demo.sh.personal.log) and [group project](./cmdline-demo.sh.group.log).

## Infrastructure schema
How does the basic infrastructure looks like?
* single VM (ubuntu-jammy)
  * VM firewall opening port 22
  * VM SSH keypair generated locally and pubkey uploaded to cloud
* private subnet and network (skipped for personal projects where shared entities are used)
* router to external internet (skipped for personal projects where shared entities are used)
* public floating ip address

![basic-infrastructure.png](/clouds/common/pictures/basic-infrastructure.png)
