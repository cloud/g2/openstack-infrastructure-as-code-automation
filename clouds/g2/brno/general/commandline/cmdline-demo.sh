#!/usr/bin/env bash
# e-INFRA CZ G2 openstack command-line demo - personal/group project
# Usage: cmdline-demo.sh [ostack-entities-prefix/profile-name]
#

SCRIPT_DIR=$(dirname $(readlink -f $0))
#############################################################################
# variables
#############################################################################
CLOUD_ENV_NAME="g2-prod-brno"
OPENSTACK_BIN="${OPENSTACK_BIN:-openstack}"
ENTITIES_PREFIX="${1:-"${USER}_$(hostname)"}"
EXTERNAL_NETWORK_NAME="external-ipv4-general-public"
KEYPAIR_NAME="${ENTITIES_PREFIX}-demo-keypair"
##NETWORK_NAME="${ENTITIES_PREFIX}-demo-network"
##SUBNET_NAME="${ENTITIES_PREFIX}-demo-subnet"
SUBNET_CIDR="${SUBNET_CIDR:-"192.168.222.0/24"}"
SERVER_NAME="${ENTITIES_PREFIX}-demo-server"
FLAVOR_NAME="${FLAVOR_NAME:-"e1.medium"}"
IMAGE_NAME="${IMAGE_NAME:-"ubuntu-jammy-x86_64"}"
VM_LOGIN="${VM_LOGIN:-"ubuntu"}"
##ROUTER_NAME="${ENTITIES_PREFIX}-demo-router"
FIP_FILE="${ENTITIES_PREFIX}-demo-fip.txt"
SECGROUP_NAME="${ENTITIES_PREFIX}-demo-secgroup"
SSH_KEYPAIR_DIR="${HOME}/.ssh/${CLOUD_ENV_NAME}"
EXTRA_VOLUME_SIZE_GB=${EXTRA_VOLUME_SIZE_GB:-"10"}
EXTRA_VOLUME_NAME="${ENTITIES_PREFIX}-demo-volume"
EXTRA_VOLUME_TYPE="${EXTRA_VOLUME_TYPE:-""}"
SERVER_CREATE_ADDITIONAL_ARGS="${SERVER_CREATE_ADDITIONAL_ARGS:-""}"
SERVER_EPHEMERAL_DISK_SIZE="${SERVER_EPHEMERAL_DISK_SIZE:-"0"}"

declare -A ROUTER_NAME_ARR
ROUTER_NAME_ARR[personal]="${ROUTER_NAME:-"internal-ipv4-general-private"}"
ROUTER_NAME_ARR[group]="${ROUTER_NAME:-"${ENTITIES_PREFIX}-demo-router"}"
declare -A NETWORK_NAME_ARR
NETWORK_NAME_ARR[personal]="${NETWORK_NAME:-"internal-ipv4-general-private"}"
NETWORK_NAME_ARR[group]="${NETWORK_NAME:-"${ENTITIES_PREFIX}-demo-network"}"
declare -A SUBNET_NAME_ARR
SUBNET_NAME_ARR[personal]="${SUBNET_NAME:-"internal-ipv4-general-private-172-16-0-0"}"
SUBNET_NAME_ARR[group]="${SUBNET_NAME:-"${ENTITIES_PREFIX}-demo-subnet"}"

#############################################################################
# functions
#############################################################################
source ${SCRIPT_DIR}/../../../../common/lib.sh.inc

#############################################################################
# main steps
#############################################################################
log_section "Using commandline tools:"
report_tools || myexit 1

log_section "Using OpenStack cloud:"
${OPENSTACK_BIN} version show -fcsv | grep identity || myexit 1

# detect project type (group/personal) --------------------------------------
project_type=group
if prj_name=$(is_personal_project); then
  project_type=personal
fi
NETWORK_NAME="${NETWORK_NAME_ARR[${project_type}]}"
SUBNET_NAME="${SUBNET_NAME_ARR[${project_type}]}"
ROUTER_NAME="${ROUTER_NAME_ARR[${project_type}]}"
log "Using OpenStack ${project_type} project named: ${prj_name}"

# delete objects (from previous run) ----------------------------------------
log_section "Delete previously created objects (delete_objects_${project_type}_project)"
delete_objects_${project_type}_project
# ---------------------------------------------------------------------------

log_section "List currently allocated objects"
list_objects
# ---------------------------------------------------------------------------

log_section_keypress "Create (generate) locally SSH keypair, upload public SSH key to cloud"
mkdir -p "${SSH_KEYPAIR_DIR}"
chmod 700 "${SSH_KEYPAIR_DIR}"
if [ -s "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}" -a -s "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}.pub" ]; then
  log "Reusing already existing SSH keypair at ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}"
else
  ssh-keygen -t rsa -b 4096 -f "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}"
fi
${OPENSTACK_BIN} keypair create --type ssh --public-key "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}.pub" "${KEYPAIR_NAME}"
ls -la ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}*
# ---------------------------------------------------------------------------

log_section_keypress "Create cloud security groups (custom VM firewall) to allow outgoing traffic and incomming SSH traffic on port 22"
${OPENSTACK_BIN} security group create --description "${ENTITIES_PREFIX} demo default security group" "${SECGROUP_NAME}"
${OPENSTACK_BIN} security group rule create --ingress --proto tcp --remote-ip 0.0.0.0/0 --dst-port 22 "${SECGROUP_NAME}"
${OPENSTACK_BIN} security group rule create --egress --proto tcp --remote-ip 0.0.0.0/0 --dst-port 1:65535 "${SECGROUP_NAME}"
# ---------------------------------------------------------------------------

if [ "${project_type}" == "group" ]; then
  log_section_keypress "Create cloud private network and subnet, so far isolated (CIDR:${SUBNET_CIDR})"
  ${OPENSTACK_BIN} network create "${NETWORK_NAME}"
  NETWORK_ID=$(${OPENSTACK_BIN} network show "${NETWORK_NAME}" -f value -c id)
  ${OPENSTACK_BIN} subnet create "${SUBNET_NAME}" --network "${NETWORK_ID}" --subnet-range "${SUBNET_CIDR}" --dns-nameserver 8.8.4.4 --dns-nameserver 8.8.8.8
else
  NETWORK_ID=$(${OPENSTACK_BIN} network show "${NETWORK_NAME}" -f value -c id)
  log_section_keypress "Re-use existing network (${NETWORK_NAME}) and subnet (${SUBNET_NAME})"
fi
# ---------------------------------------------------------------------------

if [ "${EXTRA_VOLUME_SIZE_GB}" -gt 0 ]; then
  log_keypress "Create cloud VM extra volume \"${EXTRA_VOLUME_NAME}\" with following configuration:\n" \
               "  size: ${EXTRA_VOLUME_SIZE_GB} GB, volume type: ${EXTRA_VOLUME_TYPE}"
  ${OPENSTACK_BIN} volume create ${EXTRA_VOLUME_TYPE:+--type=${EXTRA_VOLUME_TYPE}} --size "${EXTRA_VOLUME_SIZE_GB}" ${EXTRA_VOLUME_NAME}
fi
# ---------------------------------------------------------------------------

if [ -n "${SERVER_EPHEMERAL_DISK_SIZE}" -a "${SERVER_EPHEMERAL_DISK_SIZE}" -gt "0" ]; then
  SERVER_CREATE_ADDITIONAL_ARGS="${SERVER_CREATE_ADDITIONAL_ARGS} --ephemeral=size=${SERVER_EPHEMERAL_DISK_SIZE}"
fi
log_section_keypress "Create cloud VM instance \"${SERVER_NAME}\" with following configuration:\n" \
                     "  flavor: ${FLAVOR_NAME}, image/os: ${IMAGE_NAME}, network: ${NETWORK_NAME}\n" \
                     "  keypair: ${KEYPAIR_NAME}, sec-group/firewall: ${SECGROUP_NAME})" \
                     "  additional arguments: ${SERVER_CREATE_ADDITIONAL_ARGS}"
${OPENSTACK_BIN} server create --flavor "${FLAVOR_NAME}" --image "${IMAGE_NAME}" \
                        --network "${NETWORK_ID}" --key-name "${KEYPAIR_NAME}" \
                        --security-group "${SECGROUP_NAME}" ${SERVER_CREATE_ADDITIONAL_ARGS} "${SERVER_NAME}"
SERVER_ID=$(${OPENSTACK_BIN} server show "${SERVER_NAME}" -f value -c id)

log_section "Wait for VM instance \"${SERVER_NAME}\" being ACTIVE"
vm_wait_for_status "${SERVER_NAME}" "ACTIVE"

if [ "${EXTRA_VOLUME_SIZE_GB}" -gt 0 ]; then
  log_section_keypress "Attach extra volume \"${EXTRA_VOLUME_NAME}\" (${EXTRA_VOLUME_SIZE_GB} GB) to VM \"${SERVER_NAME}\""
  ${OPENSTACK_BIN} server add volume ${SERVER_NAME} ${EXTRA_VOLUME_NAME} --device /dev/sdb
fi
# ---------------------------------------------------------------------------

if [ "${project_type}" == "group" ]; then
  log_section "Route VM from internal software defined networking outside"
  log_keypress "  1] Create route, associate router with external provider network and internal subnet (${SUBNET_CIDR})"
  ${OPENSTACK_BIN} router create "${ROUTER_NAME}"
  ${OPENSTACK_BIN} router set "${ROUTER_NAME}" --external-gateway "${EXTERNAL_NETWORK_NAME}"
  ${OPENSTACK_BIN} router add subnet "${ROUTER_NAME}" "${SUBNET_NAME}"
else
  log "  1] Reuse existing router ${ROUTER_NAME} (may not be visible from personal projects)"
fi
# ---------------------------------------------------------------------------

log_keypress "  2] Allocate single FIP (floating ip) from external provider network"
FIP=$(${OPENSTACK_BIN} floating ip create "${EXTERNAL_NETWORK_NAME}" -f value -c name)
if [ -n "${FIP}" ]; then
  echo "${FIP}" > "${FIP_FILE}"
  echo "Successfully obtained public ipv4 floating IP adress (FIP): ${FIP}"

  log "  3] Associate selected FIP with created VM"
  ${OPENSTACK_BIN} server add floating ip "${SERVER_NAME}" "${FIP}"

  log_section "VM server instance access tests"
  log_keypress "  1] TCP ping (ncat -z ${FIP} 22)"
  test_vm_access_ncat "${FIP}"
  log_keypress "  2] SSH command (ssh -i ${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME} ${VM_LOGIN}@${FIP})"
  ssh-keygen -R ${FIP} &>/dev/null
  ssh -o StrictHostKeyChecking=no -i "${SSH_KEYPAIR_DIR}/id_rsa.${KEYPAIR_NAME}" "${VM_LOGIN}@${FIP}" 'echo "";uname -a;uptime; echo "VM access succeeded!"'
else
  log "Unable to allocate FIP address, VM is created but not accessible from internet."
fi
# ---------------------------------------------------------------------------

log_section_keypress "Object summary in profile ${ENTITIES_PREFIX}"
list_objects
# ---------------------------------------------------------------------------

log_section_keypress "Teardown of the objects (delete_objects_${project_type}_project)" \
                     "(Interrupt with CTRL-C if you want to keep the created infrastructure and skip its destruction)"
delete_objects_${project_type}_project
