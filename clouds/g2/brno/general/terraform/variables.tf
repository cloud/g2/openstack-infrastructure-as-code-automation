variable "router_creation_enable" {
  description = "Create dedicated router instance. true/false ~ create new / reuse existing personal router"
  default     = true
}

variable "internal_network_creation_enable" {
  description = "Create dedicated internal network. true/false ~ create new / reuse existing personal network"
  default     = true
}

variable "internal_network_name" {
  description = "Internal network name. Either dedicated new network or existing personal network name"
  default     = "<var.infra_name>_network"
}

variable "internal_subnet_creation_enable" {
  description = "Create dedicated subnet instance. true/false ~ create new / reuse existing personal subnet"
  default     = true
}

variable "internal_subnet_name" {
  description = "Internal network subnet name. Either dedicated new subnet or existing personal subnet name"
  default     = "<var.infra_name>_subnet"
}
