# Floating IPs (only for bastion node)
resource "openstack_networking_floatingip_v2" "bastion_fip" {
  pool  = var.public_external_network
}

resource "openstack_compute_floatingip_associate_v2" "bastion_fip_associate" {
  floating_ip = openstack_networking_floatingip_v2.bastion_fip.address
  instance_id = openstack_compute_instance_v2.bastion.id
}

# Ports
resource "openstack_networking_port_v2" "bastion_port" {
  name               = "${var.infra_name}-${var.bastion_name}-port"
  network_id         = var.internal_network_creation_enable ? openstack_networking_network_v2.network_default[0].id : data.openstack_networking_network_v2.internal_shared_personal_network[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = var.internal_subnet_creation_enable ? openstack_networking_subnet_v2.subnet_default[0].id : data.openstack_networking_subnet_v2.internal_shared_personal_subnet[0].id
  }
}
