users:
  - default
  - name: ubuntu
    shell: /bin/bash
    ssh_authorized_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC5fFLKPzxna7fq6kh1CHaIQFnpqXybqLqGs4ZpTDbIrEn7xjCsdyxMm9dcptyS0t6BzXO56BlJyYsR1GWo4rp3g8rMmb9u6/oHmMwgn7G/GLgsaAAO5XHW0A3UEJl3JHfCQLHkN1APQ4dy7gNTG24ahH/pcyr4rV0SsjPUCqFqkSMDZxRgfllNGftxWVHR2fYfPALLrGdhR/SjNSIs3pwBIUXaSfF3aBLsjeGBj4y5YsiR9yI3y2gUmpURROofTvtE7Fp8OIgmWCVqRe70CKDbl17HFbz3FIqYwZLAQHILcp1M45zV8koSOjW5+3C/ZJYzBKOnw/a/1Cw3uHFDrZfRqKLMP/gagnoEPRHjfmUsJ3UJO0eXDCXmnH7F48xBI76CgxYl039/SMmJ2mR0KqAHGnwqVmJI3yBGyK+Z4iEwk+JVDLEB14RHiMp2/I/tYpDWFE1IOigFFNLdfaZrVFY1/fD+yGGyFUO1Wo+CKb8tpndLB4H3Yj2MLRDP/aNpLC4M7Aru7hWnUF81aE/VUAqR6CP2vsHzlAOmH08pOlP9FVITinmJqzBL15l+W7q0Rhh4WBRO4ixlrtRJDNL2wm0vf+GiJnXligFtZ7Cw8bk/LcAe37WqcTl0xLKDyPSw4SvWOC2aE6BVuJjPAhoUUcBaNzoBa7lf4eb+FS4tquTZlQ== freznicek@LenovoThinkCentreE73
disk_setup:
  /dev/sdb:
    table_type: gpt
    layout: true
    overwrite: true
fs_setup:
- label: extra_data
  filesystem: ext4
  device: /dev/sdb1
  cmd: mkfs -t %(filesystem)s -L %(label)s %(device)s
runcmd:
  - mkdir -p /mnt/data
mounts:
  - ["/dev/sdb1", "/mnt/data"]
ssh_pwauth: false
