
LOOP_CNT=${1:-10}

TERRAFORM="${TERRAFORM:-"terraform"}"
TERRAFORM_COMMON_ARGS="-no-color"
STEPS="${STEPS:-"init validate plan1 apply1 plan2 apply2 destroy1 destroy2"}"
declare -A actions
actions[init]="init"
actions[validate]="validate"
actions[plan]="plan --out plan"
actions[apply]="apply plan"
actions[destroy]="destroy -auto-approve"

declare -A timeouts
timeouts[init]=60
timeouts[validate]=60
timeouts[plan]=1210
timeouts[apply]=600
timeouts[destroy]=300


function duration_human() {
    local secs="$1"
    if [[ "${secs}" -lt 60 ]]; then
        echo "${secs}s"
    elif [[ "${secs}" -lt $((60*60)) ]]; then
        echo "$((${secs} / 60))m$((${secs} % 60))s"
    else
        echo "$((${secs} / (60*60)))h$(( ( ${secs} % (60*60) ) /60 ))m$((${secs} % 60))s"
    fi
}

rm -f test-round-*.log

for ((i=0;i<${LOOP_CNT};i++)); do
  i_start_seconds=$SECONDS
  i_logfile="$(printf "test-round-%004d.log" $i)"
  echo -n "round ${i}: "
  :> "${i_logfile}"
  i_step_err_cnt=0
  for i_step in ${STEPS}; do
    i_action="$(echo ${i_step} | grep -Eo '[a-z]+')"
    echo -n "${i_step}:"
    echo -e "\ntimeout ${timeouts[${i_action}]} ${TERRAFORM} ${actions[${i_action}]} ${TERRAFORM_COMMON_ARGS}" >> "${i_logfile}"
    timeout ${timeouts[${i_action}]} ${TERRAFORM} ${actions[${i_action}]} ${TERRAFORM_COMMON_ARGS} >> "${i_logfile}" 2>&1
    i_ecode=$?
    [ "${i_ecode}" != "0" ] && let i_step_err_cnt++
    echo -n "${i_ecode} "
  done
  echo " (dur: $(duration_human $(( $SECONDS - $i_start_seconds )))/$(duration_human $SECONDS), step/log_err_cnt: ${i_step_err_cnt}/$(grep -Eic 'error|fail' ${i_logfile}))"
  [ "${i_ecode}" != "0" -o -f "test-looping.stop" ] && \
    break
done
echo "Done."
