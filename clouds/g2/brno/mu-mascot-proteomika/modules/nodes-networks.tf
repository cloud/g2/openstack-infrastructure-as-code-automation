# Floating IPs
resource "openstack_networking_floatingip_v2" "nodes_fips" {
  count    = var.nodes_count
  pool     = var.public_external_network
}

resource "openstack_networking_floatingip_associate_v2" "nodes_fips_associations" {
  count       = var.nodes_count
  floating_ip = element(openstack_networking_floatingip_v2.nodes_fips.*.address, count.index)
  port_id     = element(openstack_networking_port_v2.nodes_ports.*.id, count.index)
}
