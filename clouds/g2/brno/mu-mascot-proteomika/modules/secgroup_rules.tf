##################################
# Define Network Security Groups #
##################################


resource "openstack_networking_secgroup_v2" "secgroup_default" {
  name        = "${var.infra_name}_security_group"
  description = "${var.infra_name} Security group"
}


# Allow all internal TCP & UDP
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_allinternaltcp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 32767
  remote_ip_prefix  = var.internal_network_cidr
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_allinternaludp4" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 22
  port_range_max    = 32767
  remote_ip_prefix  = var.internal_network_cidr
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

# ICMP from MUNI
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_icmp4_muni" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = "147.251.0.0/16"
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

# SSH rules
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_ssh4" {
  count             = length(var.nodes_secgroup_ssh_rule_remote_ip_prefixes)
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.nodes_secgroup_ssh_rule_remote_ip_prefixes[count.index]
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

# HTTP
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_http4" {
  count             = length(var.nodes_secgroup_http_rule_remote_ip_prefixes)
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = var.nodes_secgroup_http_rule_remote_ip_prefixes[count.index]
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}

# HTTPS
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_https4" {
  count             = length(var.nodes_secgroup_https_rule_remote_ip_prefixes)
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = var.nodes_secgroup_https_rule_remote_ip_prefixes[count.index]
  security_group_id = openstack_networking_secgroup_v2.secgroup_default.id
}




