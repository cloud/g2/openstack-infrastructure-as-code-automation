####################
# Define instances #
####################

data "openstack_images_image_v2" "nodes_image" {
  name        = var.nodes_image
}

resource "openstack_compute_instance_v2" "nodes" {
  count           = var.nodes_count
  name            = "${var.infra_name}-${var.nodes_name}-${count.index+1}"
  image_name      = var.nodes_image
  flavor_name     = var.nodes_flavor
  key_pair        = openstack_compute_keypair_v2.pubkey.name
  security_groups = [openstack_networking_secgroup_v2.secgroup_default.name]
  user_data       = "#cloud-config\nhostname: ${var.infra_name}-${var.nodes_name}-${count.index+1}.local\n${file("${path.module}/nodes-cloudinit.txt")}"

  network {
    uuid = data.openstack_networking_network_v2.internal_network.id
    port = element(openstack_networking_port_v2.nodes_ports.*.id, count.index)
  }

  block_device {
    uuid                  = data.openstack_images_image_v2.nodes_image.id
    source_type           = "image"
    volume_size           = var.nodes_volume_size
    destination_type      = "volume"
    boot_index            = 0
    delete_on_termination = true
  }
  block_device {
    source_type           = "blank"
    destination_type      = "local"
    volume_size           = 500
    boot_index            = -1
    delete_on_termination = true
  }
}
