# use existing group project networking

data "openstack_networking_network_v2" "external_network" {
  name = var.public_external_network
}

data "openstack_networking_network_v2" "internal_network" {
  name = var.internal_network_name
}

data "openstack_networking_subnet_v2" "internal_subnet" {
  name  = var.internal_subnet_name
}

resource "openstack_networking_port_v2" "nodes_ports" {
  count              = var.nodes_count
  name               = "${var.infra_name}_${var.nodes_name}_port_${count.index+1}"
  network_id         = data.openstack_networking_network_v2.internal_network.id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  mac_address        = count.index < length(var.nodes_port_mac_address) ? var.nodes_port_mac_address[count.index] : null
  fixed_ip {
    subnet_id        = data.openstack_networking_subnet_v2.internal_subnet.id
  }
}
