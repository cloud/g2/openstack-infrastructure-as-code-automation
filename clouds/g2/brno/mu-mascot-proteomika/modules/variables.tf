#########################
# general configuration #
#########################
variable "infra_name" {
  description = "Infrastructure (profile) name. Used as a name prefix. Must match [a-zA-Z0-9-]+ regexp."
  default     = "general-tf-demo"
}

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "internal_network_cidr" {
  description = "Internal network address, use CIDR notation"
  default     = "10.10.10.0/24"
}

variable "public_external_network" {
  description = "Cloud public external network name"
  default     = "external-ipv4-general-public"
}

variable "internal_network_name" {
  description = "Internal network name."
  default     = "group-project-network"
}

variable "internal_subnet_name" {
  description = "Internal network subnet name."
  default     = "group-project-network-subnet"
}


variable "nodes_count" {
  default = 1
}

variable "nodes_name" {
  description = "Name of the nodes. Must match [a-zA-Z0-9-]+ regexp."
  default = "server"
}

variable "nodes_flavor" {
  default = "standard.large"
}

variable "nodes_image" {
  description = "nodes OS: Image name"
  default     = "ubuntu-jammy-x86_64"
}

variable "nodes_ssh_user_name" {
  default = "ubuntu"
}

variable "nodes_volume_size" {
  description = "The size of the volume to create (in gigabytes) for root filesystem. "
  default     = "20"
}

variable "nodes_extra_volume_size" {
  description = "The size of the volume to create (in gigabytes) for extra data. 0 to disable extra volume."
  default     = "10"
}

variable "nodes_extra_volume_type" {
  description = "The type of extra volume."
  default     = null
}

variable "nodes_secgroup_ssh_rule_remote_ip_prefixes" {
  type        = list(string)
  description = "Allow SSH traffic from following ip prefixes."
  default     = ["147.251.0.0/16", "147.251.159.224/28", "147.251.61.192/29",
                 "147.251.159.0/26", "147.251.61.200/30", "147.251.61.206/32",
                 "147.251.61.204/31"]
}

variable "nodes_secgroup_http_rule_remote_ip_prefixes" {
  type        = list(string)
  description = "Allow HTTP traffic from following ip prefixes."
  default     = ["10.56.94.112/28", "147.251.61.206/32", "147.251.61.204/31",
                 "10.56.94.96/28", "10.56.94.64/28", "147.251.159.0/26",
                 "10.56.94.16/28", "10.56.94.80/28", "147.251.61.192/29",
                 "147.251.159.224/28", "147.251.61.200/30"]
}

variable "nodes_secgroup_https_rule_remote_ip_prefixes" {
  type        = list(string)
  description = "Allow HTTPS traffic from following ip prefixes."
  default     = ["10.56.94.96/28", "147.251.159.0/26", "10.56.94.16/28",
                 "147.251.61.206/32", "147.251.61.204/31", "147.251.61.200/30",
                 "147.251.159.224/28", "10.56.94.80/28", "10.56.94.112/28",
                 "147.251.61.192/29", "10.56.94.64/28"]
}

variable "nodes_port_mac_address" {
  type        = list(string)
  description = "List of MAC addresses used for openstack ports "
  default     = []
}
