# Terraform demonstration

This Terraform module is able to demonstrate creation of mascot infrastructure declarative way in the mu-mascot-proteomika group project.


## Cloud VM configuration during system boot using cloud-init
Terraform demonstrates [how to configure VM servers on creation with cloud-init](modules/common/nodes-cloudinit.txt):
 - Add ssh keys, disable SSH password auth
 - Create partition and filesystemand mount extra data from extra volume

## Terraform demo requirements

### locally installed tools
 * Terraform
 * OpenStack command-line client

### re-using prepared container
 * install docker or podman
 * use registry.gitlab.ics.muni.cz:443/cloud/g2/common-cloud-entities:v1.1.0 image following way:

```console
ubuntu@freznicek-jammy:~ 0 $ podman run -it -v $HOME/openstack-infrastructure-as-code-automation:/root/openstack-infrastructure-as-code-automation -v $HOME/c/g2-prod-brno-freznicek-mu-mascot-proteomika-reader-member.sh.inc:/root/g2-prod-brno-freznicek-mu-mascot-proteomika-reader-member.sh.inc:ro  registry.gitlab.ics.muni.cz:443/cloud/g2/common-cloud-entities:v1.1.0
root@95916bf18029:/# cd
root@95916bf18029:~# source ~/g2-prod-brno-freznicek-mu-mascot-proteomika-reader-member.sh.inc
root@95916bf18029:~# cd openstack-infrastructure-as-code-automation/clouds/g2/brno/
root@95916bf18029:~/openstack-infrastructure-as-code-automation/clouds/g2/brno# openstack --version
openstack 6.5.0
root@95916bf18029:~/openstack-infrastructure-as-code-automation/clouds/g2/brno# terraform --version
Terraform v1.7.4
...
```

## Using the terraform demo

1. Clone the repository.
1. Load you OpenStack application credentials to environment variables `source project_openrc.sh.inc`
1. Override any infrastructure variables in [main.tf](main.tf) file if needed. Full set of variables can be found in [modules/variables.tf](modules/variables.tf).
1. In the [terraform root directory](/clouds/g2/brno/mu-mascot-proteomika/terraform) run following commands to initiate and validate environment
   * `terraform init`
   * `terraform validate`
1. In the [same directory](/clouds/g2/brno/mu-mascot-proteomika/terraform) run commands to deploy cloud infrastructure
   * `terraform plan --out plan`
   * `terraform apply plan`
1. Once you need to change the infrastructure, first modify the infrastructure declaration and repeat above steps to deploy changes.
1. Cloud resources can be deleted with `terraform destroy`.

Detailed terminal transcripts show how to run terraform demo:
```console
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ pwd
/home/freznicek/prjs/muni/sources/gitlab.ics.muni.cz/_g2-ostack/openstack-infrastructure-as-code-automation.git/clouds/g2/brno/mu-mascot-proteomika
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ ls
main.tf  modules  README.md
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ source ~/c/g2-prod-brno-freznicek-mu-mascot-proteomika-reader-member.sh.inc
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ openstack version show | grep identity
| Brno1       | identity       | 3.14    | CURRENT   | https://identity.brno.openstack.cloud.e-infra.cz/v3/           | None             | None             |
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ terraform init

Initializing the backend...
Initializing modules...

Initializing provider plugins...
- Reusing previous version of terraform-provider-openstack/openstack from the dependency lock file
- Using previously-installed terraform-provider-openstack/openstack v2.1.0

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ terraform validate
Success! The configuration is valid.

[freznicek@lnovo-t14 mu-mascot-proteomika 0]$ terraform plan --out plan
...
Plan: 41 to add, 0 to change, 0 to destroy.

───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

Saved the plan to: plan

To perform exactly these actions, run the following command to apply:
    terraform apply "plan"
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ ll plan
-rw-r--r--. 1 freznicek freznicek 12434  6. srp 17.31 plan
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ terraform apply "plan"
module.toplevel.openstack_compute_keypair_v2.pubkey: Creating...
module.toplevel.openstack_blockstorage_volume_v3.nodes_volumes[0]: Creating...
module.toplevel.openstack_networking_secgroup_v2.secgroup_default: Creating...
module.toplevel.openstack_blockstorage_volume_v3.nodes_extra_volumes[0]: Creating...
module.toplevel.openstack_networking_floatingip_v2.nodes_fips[0]: Creating...
module.toplevel.openstack_networking_secgroup_v2.secgroup_default: Creation complete after 1s [id=c3c25594-4bca-4539-9b73-2c6d87d23f58]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_icmp4_muni: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_allinternaludp4: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[0]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_allinternaltcp4: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[5]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[8]: Creating...
module.toplevel.openstack_compute_keypair_v2.pubkey: Creation complete after 1s [id=mu-mascot-proteomika-keypair]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[7]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_allinternaludp4: Creation complete after 1s [id=b9756fdc-0468-4b93-a66a-aded673c4936]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[4]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_icmp4_muni: Creation complete after 1s [id=2eab0ea9-c649-4654-be8f-7d6415c8176f]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[5]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[0]: Creation complete after 1s [id=88ccf517-18d0-4c01-9a26-91fd3bef4350]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[2]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[5]: Creation complete after 1s [id=bf0bd802-3337-4a08-bbb8-e2f86d35a70b]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[1]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[8]: Creation complete after 2s [id=da905d62-9c00-4cfd-b370-4f4a07c92b09]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[0]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_allinternaltcp4: Creation complete after 2s [id=43a12f16-3591-424f-993f-0a8015064d48]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[1]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[7]: Creation complete after 2s [id=4a9bed42-e0c5-45f7-ba3b-3c63d1aeb287]
module.toplevel.openstack_networking_port_v2.nodes_ports[0]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[4]: Creation complete after 1s [id=bf6848ba-4fd4-4757-bedc-212fdf69ed59]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[3]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[5]: Creation complete after 1s [id=da1c1167-4941-4f52-b592-af7f19b17691]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[10]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[2]: Creation complete after 1s [id=b022724c-793c-481f-9d29-8eb8f59d3142]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[2]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[1]: Creation complete after 2s [id=e02646de-d21d-4ced-ab90-c57f46d64c8d]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[4]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[0]: Creation complete after 1s [id=05068158-6a84-44d2-aaed-34500d25a890]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[9]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[1]: Creation complete after 1s [id=0906b5fa-9b0f-4683-a03d-bf6a0a32ca7e]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[3]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[3]: Creation complete after 1s [id=ae71872a-2fc5-4e2f-8488-24da1badbca2]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[6]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[10]: Creation complete after 2s [id=74f58dd9-23ce-478f-8453-e74f2cfc0011]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[6]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[2]: Creation complete after 2s [id=628ae8da-1ab1-4cbd-a7cf-634a0a3fb820]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[7]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[4]: Creation complete after 1s [id=d6b902ee-aa41-411a-bfd5-4bdba067d18d]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[9]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[9]: Creation complete after 1s [id=dbcf7a01-57bb-4127-b2bf-8491d3ffbdf1]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[5]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[3]: Creation complete after 1s [id=1a9f1a92-853a-4344-ab27-52b30890e0e4]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[3]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_https4[6]: Creation complete after 2s [id=0caa90e7-3700-4f04-a26f-31307628049d]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[6]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_ssh4[6]: Creation complete after 1s [id=3bcf0426-a7fc-4a10-82a1-4643fa376ae8]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[4]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[7]: Creation complete after 1s [id=331ddb5a-d9fe-4f04-b3c3-956f838d0c23]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[2]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[9]: Creation complete after 1s [id=cb77a395-e7c5-4cb8-956f-12557da3b405]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[0]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[5]: Creation complete after 1s [id=9ea10bbd-9727-4cca-a5b2-1ccc85cb3016]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[1]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[3]: Creation complete after 2s [id=e0534b9e-4ca3-4c70-9fc0-08aa44a22241]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[10]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[6]: Creation complete after 1s [id=856e537b-4374-4b1e-9030-66b898afeb6c]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[8]: Creating...
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[4]: Creation complete after 1s [id=907c9389-971d-49d5-9637-9f0b04f28e71]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[2]: Creation complete after 1s [id=59010c8e-6276-46f2-b92b-04b03302fde6]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[0]: Creation complete after 1s [id=9ecbcfed-a7be-45cd-9b28-da2380dd8e13]
module.toplevel.openstack_networking_floatingip_v2.nodes_fips[0]: Creation complete after 7s [id=d951077c-6422-4415-9ebc-56207a3673f6]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[1]: Creation complete after 2s [id=19b12f85-4713-442d-9143-7b1fd468c751]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[10]: Creation complete after 1s [id=1930c299-b8e6-4877-bf04-a53fe59f0840]
module.toplevel.openstack_networking_secgroup_rule_v2.secgroup_rule_http4[8]: Creation complete after 1s [id=3a6cff1e-de6f-4b1c-aeaf-2a561bc66d65]
module.toplevel.openstack_networking_port_v2.nodes_ports[0]: Creation complete after 6s [id=8ab56c81-e3ad-4564-a8af-dba2ea2bb0c0]
module.toplevel.openstack_networking_floatingip_associate_v2.nodes_fips_associations[0]: Creating...
module.toplevel.openstack_compute_instance_v2.nodes[0]: Creating...
module.toplevel.openstack_blockstorage_volume_v3.nodes_volumes[0]: Still creating... [10s elapsed]
module.toplevel.openstack_blockstorage_volume_v3.nodes_extra_volumes[0]: Still creating... [10s elapsed]
module.toplevel.openstack_networking_floatingip_associate_v2.nodes_fips_associations[0]: Creation complete after 2s [id=d951077c-6422-4415-9ebc-56207a3673f6]
module.toplevel.openstack_blockstorage_volume_v3.nodes_volumes[0]: Creation complete after 12s [id=6b16d586-c92e-457b-8131-2105fec0bb69]
module.toplevel.openstack_blockstorage_volume_v3.nodes_extra_volumes[0]: Creation complete after 12s [id=80547b27-25dc-46e5-950a-72b3d0d3dbe9]
module.toplevel.openstack_compute_instance_v2.nodes[0]: Still creating... [10s elapsed]
module.toplevel.openstack_compute_instance_v2.nodes[0]: Still creating... [20s elapsed]
module.toplevel.openstack_compute_instance_v2.nodes[0]: Creation complete after 24s [id=4e6416d6-57cb-4a90-a045-c60211cf9ce7]
module.toplevel.openstack_compute_volume_attach_v2.nodes_extra_volumes_attachments[0]: Creating...
module.toplevel.openstack_compute_volume_attach_v2.nodes_extra_volumes_attachments[0]: Creation complete after 6s [id=4e6416d6-57cb-4a90-a045-c60211cf9ce7/80547b27-25dc-46e5-950a-72b3d0d3dbe9]

Apply complete! Resources: 41 added, 0 changed, 0 destroyed.
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$
[freznicek@lenovo-t14 mu-mascot-proteomika 0]$ openstack server list
+--------------------------------------+-------------------------------+--------+------------------------------------------------------+--------------------------+--------------------------+
| ID                                   | Name                          | Status | Networks                                             | Image                    | Flavor                   |
+--------------------------------------+-------------------------------+--------+------------------------------------------------------+--------------------------+--------------------------+
| 4e6416d6-57cb-4a90-a045-c60211cf9ce7 | mu-mascot-proteomika-server-1 | ACTIVE | group-project-network=147.251.255.36, 192.168.0.43   | N/A (booted from volume) | p3.24core-60ram-500edisk |
| 06061d96-e04d-43e8-b296-5784be641e1a | mascot_main                   | ACTIVE | group-project-network=147.251.255.212, 192.168.0.220 | N/A (booted from volume) | c3.24core-60ram          |
+--------------------------------------+-------------------------------+--------+------------------------------------------------------+--------------------------+--------------------------+

[freznicek@lenovo-t14 ~ 0]$ ssh -J ltce73 debian@147.251.255.36 'uname -a;lsblk;sudo fdisk -l'
Linux mu-mascot-proteomika-server-1 6.1.0-18-cloud-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.76-1 (2024-02-01) x86_64 GNU/Linux
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda       8:0    0   20G  0 disk
├─sda1    8:1    0 19.9G  0 part /
├─sda14   8:14   0    3M  0 part
└─sda15   8:15   0  124M  0 part /boot/efi
sdb       8:16   0  4.9T  0 disk
└─sdb1    8:17   0  4.9T  0 part /mnt/ephem_data
sdc       8:32   0  500G  0 disk
└─sdc1    8:33   0  500G  0 part /mnt/ceph_data
Disk /dev/sdb: 4.88 TiB, 5368709120000 bytes, 10485760000 sectors
Disk model: QEMU HARDDISK
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 2DDD6558-123C-4B60-8762-6C039DDE3097

Device     Start         End     Sectors  Size Type
/dev/sdb1   2048 10485759966 10485757919  4.9T Linux filesystem


Disk /dev/sdc: 500 GiB, 536870912000 bytes, 1048576000 sectors
Disk model: QEMU HARDDISK
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: DFC67142-EB69-4DAE-B144-124BCAA013A9

Device     Start        End    Sectors  Size Type
/dev/sdc1   2048 1048575966 1048573919  500G Linux filesystem


Disk /dev/sda: 20 GiB, 21474836480 bytes, 41943040 sectors
Disk model: QEMU HARDDISK
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: D822F3CD-EBBD-9941-BFA3-7AC142A9F44E

Device      Start      End  Sectors  Size Type
/dev/sda1  262144 41943006 41680863 19.9G Linux filesystem
/dev/sda14   2048     8191     6144    3M BIOS boot
/dev/sda15   8192   262143   253952  124M EFI System

Partition table entries are not in disk order.
[freznicek@lenovo-t14 ~ 0]$


# state of the infrastructure is stored in state filesystem[freznicek@lenovo-t14 mu-mascot-proteomika 2]$ ls -la *state*
-rw-r--r--. 1 freznicek freznicek 53211  6. srp 17.32 terraform.tfstate
-rw-r--r--. 1 freznicek freznicek   182  6. srp 17.31 terraform.tfstate.backup
```

### How to change configuration

* MAC address, variable `nodes_port_mac_address`
* instance flavor, variable `nodes_flavor`, you may experiment with `p3.24core-60ram-500edisk` and `p3.24core-60ram-500edisk-smt`
