terraform {
  backend "local" {}
}

module "toplevel" {
  source                  = "./modules"
  infra_name              = "mu-mascot-proteomika"

  nodes_count             = 1
  nodes_flavor            = "p3.24core-60ram-500edisk"
  nodes_image             = "debian-12-x86_64"
  nodes_ssh_user_name     = "debian"
  public_external_network = "external-ipv4-general-public"
  nodes_extra_volume_size = 5000  # extra volume size in GB
  nodes_port_mac_address  = [ "fa:16:3e:8e:3d:b9" ]
}
