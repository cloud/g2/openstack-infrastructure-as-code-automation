#!/usr/bin/env bash
# terraform wrapper for automatic personal/group project reconfiguration
# terraform.sh <arguments>
#
# <arguments> are:
# * detect-cloud
# * detect-project
# * validate-tools
# * or any valid terraform arguments

# functions
SCRIPT_DIR=$(dirname $(readlink -f $0))
source ${SCRIPT_DIR}/../../../../common/lib.sh.inc


if [[ "$1" =~ (detect-(cloud|project)|validate-tools) ]]; then
    if [ "$1" == "detect-cloud" ]; then
        log "Using OpenStack cloud:"
        openstack version show -fcsv | grep identity
    elif [ "$1" == "detect-project" ]; then
        project_type=group
        if prj_name=$(is_personal_project); then
            project_type=personal
        fi
        log "Using OpenStack ${project_type} project named: ${prj_name}"
    elif [ "$1" == "validate-tools" ]; then
        log "Using commandline tools:"
        report_tools "terraform version"
    fi
    exit $?
else
    project_type=group
    if prj_name=$(is_personal_project); then
        project_type=personal
        if [ "$1" == "plan" ]; then
            terraform "$@" --var-file=personal-projects.tfvars
        else
            terraform "$@"
        fi
    else
        terraform "$@"
    fi
fi





