# Terraform demonstration

This Terraform module is able to demonstrate creation of two most used cloud infrastructure patterns:

## Two tier infrastructure: public bastion and private VM farm

Infrastructure consist of:
 - public facing small bastion VM (sometimes called as jump VM)
 - private VM farm

![two-tier-infra.png](/clouds/common/pictures/two-tier-infra.png)

## Single tier infrastructure i.e. public facing VM farm

Infrastructure consist of:
 - public facing VM farm

![single-tier-infra.png](/clouds/common/pictures/single-tier-infra.png)


## Cloud VM configuration during system boot using cloud-init
Terraform demonstrates [how to configure VM servers on creation with cloud-init](modules/common/nodes-cloudinit.txt):
 - Add ssh keys, disable SSH password auth
 - Create partition and filesystemand mount extra data from extra volume

## Handling different project restrictions (quotas, shared networks, ...)

e-INFRA.CZ OpenStack cloud distinguishes between two project types: personal and group
([more the topic](https://docs.e-infra.cz/compute/openstack/technical-reference/brno-site/get-access/#personal-project)).
Terraform demo code with additional variable file [`personal-projects.tfvars`](./personal-projects.tfvars) shows how to support both project types i.e. how to parametrize infrastructure definition with minimal code duplications.

There is thin terraform wrapper [`terraform.sh`](./terraform.sh) abstracting the fact which project type you are on. It is not necessary to use the [`terraform.sh`](./terraform.sh) wrapper when you keep in mind that additional terraform variables file [`personal-projects.tfvars`](./personal-projects.tfvars) has to be passed at plan step when you are on personal project.

## Using the terraform demo

1. Clone the repository.
1. Load you OpenStack application credentials to environment variables `source project_openrc.sh.inc`
1. Override any infrastructure variables in [main.tf](main.tf) file if needed. Full set of variables can be found in [modules/common/variables.tf](modules/common/variables.tf).
1. In the [terraform root directory](/clouds/g2/ostrava/general/terraform) run following commands to initiate and validate environment
   * `./terraform.sh validate-tools`
   * `./terraform.sh detect-cloud`
   * `./terraform.sh detect-project`
   * `./terraform.sh init`
   * `./terraform.sh validate`
1. In the [same directory](/clouds/g2/ostrava/general/terraform) run commands to deploy cloud infrastructure
   * `./terraform.sh plan --out plan`
   * `./terraform.sh apply plan`
1. Once you need to change the infrastructure, first modify the infrastructure declaration and repeat above steps to deploy changes.
1. Cloud resources can be deleted with `./terraform.sh destroy`.

> You may use terraform directly without the [`terraform.sh`](./terraform.sh) wrapper, but then keep in your mind that just for personal project type you should pass additional variable file [`personal-projects.tfvars`](./personal-projects.tfvars) at plan step (i.e. `terraform plan --out personal-project.plan --var-file=personal-projects.tfvars`)

Detailed terminal transcripts show how to run terraform demo inside personal project ([with](./terraform-2tier_public_bastion_private_vm_farm-personal-project-wrapper.log) / [without the wrapper](./terraform-2tier_public_bastion_private_vm_farm-personal-project-no-wrapper.log)) as well as under group project([with](./terraform-2tier_public_bastion_private_vm_farm-group-project-wrapper.log) / [without the wrapper](./terraform-2tier_public_bastion_private_vm_farm-group-project-no-wrapper.log)).


## Access to the VM nodes

In single tier infrastructure you access directly the individual VM nodes via SSH on public IP addresses.
Two tier infrastructure requires the access following way:
1. Establish the connection with bastion
```sh
sshuttle -r ubuntu@<bastion-ip>
```
1. Connect directly to VM nodes via SSH on private IP addresses:
```sh
ssh  ubuntu@<vm-node-ip-from-10.10.10.0/24>
```
