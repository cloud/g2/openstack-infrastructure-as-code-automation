resource "openstack_networking_network_v2" "network_default" {
  count          = var.internal_network_creation_enable ? 1 : 0
  name           = "${var.infra_name}_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_default" {
  count           = var.internal_subnet_creation_enable ? 1 : 0
  name            = "${var.infra_name}_subnet"
  network_id      = openstack_networking_network_v2.network_default[0].id
  cidr            = var.internal_network_cidr
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "8.8.8.8"]
}

data "openstack_networking_network_v2" "external_network" {
  name = var.public_external_network
}

data "openstack_networking_network_v2" "internal_shared_personal_network" {
  count = var.internal_network_creation_enable == false ? 1 : 0
  name = var.internal_network_name
}

data "openstack_networking_subnet_v2" "internal_shared_personal_subnet" {
  count = var.internal_subnet_creation_enable == false ? 1 : 0
  name  = var.internal_subnet_name
}

resource "openstack_networking_router_v2" "router_default" {
  count               = var.router_creation_enable ? 1 : 0
  name                = "${var.infra_name}_infra-test"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.external_network.id
}

resource "openstack_networking_router_interface_v2" "router_default_interface" {
  count     = var.router_creation_enable ? 1 : 0
  router_id = openstack_networking_router_v2.router_default[0].id
  subnet_id = openstack_networking_subnet_v2.subnet_default[0].id
}

resource "openstack_networking_port_v2" "nodes_ports" {
  count              = var.nodes_count
  name               = "${var.infra_name}_${var.nodes_name}_port_${count.index+1}"
  network_id         = var.internal_network_creation_enable ? openstack_networking_network_v2.network_default[0].id : data.openstack_networking_network_v2.internal_shared_personal_network[0].id
  admin_state_up     = "true"
  security_group_ids = [openstack_networking_secgroup_v2.secgroup_default.id]
  fixed_ip {
    subnet_id = var.internal_subnet_creation_enable ? openstack_networking_subnet_v2.subnet_default[0].id : data.openstack_networking_subnet_v2.internal_shared_personal_subnet[0].id
  }
}
