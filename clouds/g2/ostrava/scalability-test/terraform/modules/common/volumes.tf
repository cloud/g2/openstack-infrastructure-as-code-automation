# node extra volumes
resource "openstack_blockstorage_volume_v3" "nodes_extra_volumes" {
  count       = (var.nodes_extra_volume_size * var.nodes_extra_volume_count > 0) ? (var.nodes_count * var.nodes_extra_volume_count) : 0
  name        = "${var.infra_name}-node-extra-volume-${count.index+1}"
  size        = var.nodes_extra_volume_size
  volume_type = var.nodes_extra_volume_type
}

resource "openstack_compute_volume_attach_v2" "nodes_extra_volumes_attachments" {
  count       = (var.nodes_extra_volume_size * var.nodes_extra_volume_count > 0) ? (var.nodes_count * var.nodes_extra_volume_count) : 0
  instance_id = element(openstack_compute_instance_v2.nodes.*.id, floor(count.index / var.nodes_extra_volume_count))
  volume_id   = element(openstack_blockstorage_volume_v3.nodes_extra_volumes.*.id, count.index)
}
