# bastion extra volumes
resource "openstack_blockstorage_volume_v3" "bastion_extra_volumes" {
  count       = (var.bastion_extra_volume_size * var.bastion_extra_volume_count > 0) ? var.bastion_extra_volume_count : 0
  name        = "${var.infra_name}-bastion-extra-volume-${count.index+1}"
  size        = var.bastion_extra_volume_size
  volume_type = var.bastion_extra_volume_type
}

resource "openstack_compute_volume_attach_v2" "bastion_extra_volumes_attachments" {
  count       = (var.bastion_extra_volume_size * var.bastion_extra_volume_count > 0) ? var.bastion_extra_volume_count : 0
  instance_id = openstack_compute_instance_v2.bastion.id
  volume_id   = element(openstack_blockstorage_volume_v3.bastion_extra_volumes.*.id, count.index)
}
