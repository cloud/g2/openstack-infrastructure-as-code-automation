terraform {
  backend "local" {}
}

module "toplevel" {
  # repeat 2-tier infrastructure N times
  count                      = 4  # add step by step 5->10->20->30->40

  # Infrastructure type
  # -------------------------------------------------------------------------
  # two tier infrastructure (2tier_public_bastion_private_vm_farm module):
  # * single public facing bastion VM
  # * <nodes_count> private HPC VM farm
  source                     = "./modules/2tier_public_bastion_private_vm_farm"

  infra_name                 = format("scalability-test-%02d", count.index+1)
  nodes_count                = 3
  nodes_flavor               = "e1.1core-2ram-30disk"
  nodes_image                = "ubuntu-jammy-x86_64"
  bastion_flavor             = "g2.tiny"
  bastion_image              = "ubuntu-jammy-x86_64"
  public_external_network    = "external-ipv4-general-public"
  nodes_extra_volume_size    = 10
  nodes_extra_volume_count   = 3
  bastion_extra_volume_size  = 10
  bastion_extra_volume_count = 3


  # OpenStack project type:
  # -------------------------------------------------------------------------
  # root variables wired 1:1 to "toplevel" module to be able to toggle between
  # group and personal project infrastructure
  router_creation_enable           = var.router_creation_enable
  internal_network_creation_enable = var.internal_network_creation_enable
  internal_network_name            = var.internal_network_name
  internal_subnet_creation_enable  = var.internal_subnet_creation_enable
  internal_subnet_name             = var.internal_subnet_name
}
