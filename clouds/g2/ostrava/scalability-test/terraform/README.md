# OpenStack scalability test terraform demonstration

This Terraform module is able to demonstrate creation of huge cloud infrastructure:

## Two tier infrastructure: public bastion and private VM farm

Infrastructure consist of:
 - public facing small bastion VM (sometimes called as jump VM)
 - private VM farm

![two-tier-infra.png](/clouds/common/pictures/two-tier-infra.png)


## Cloud VM configuration during system boot using cloud-init
Terraform demonstrates [how to configure VM servers on creation with cloud-init](modules/common/nodes-cloudinit.txt):
 - Add ssh keys, disable SSH password auth
 - Create partition and filesystemand mount extra data from extra volume


## Using the terraform demo

1. Clone the repository.
1. Load you OpenStack application credentials to environment variables `source project_openrc.sh.inc`
1. Override any infrastructure variables in [main.tf](main.tf) file if needed. Full set of variables can be found in [modules/common/variables.tf](modules/common/variables.tf).
1. In the [terraform root directory](/clouds/g2/ostrava/general/terraform) run following commands to initiate and validate environment
   * `terraform init`
   * `terraform validate`
1. In the [same directory](/clouds/g2/ostrava/general/terraform) run commands to deploy cloud infrastructure
   * `terraform plan --out plan`
   * `terraform apply plan`
1. Once you need to change the infrastructure, first modify the infrastructure declaration and repeat above steps to deploy changes.
1. Cloud resources can be deleted with `terraform destroy`.

Detailed terminal transcripts show how to run [scalability terraform demo](./terraform-2tier_public_bastion_private_vm_farm.log).


## Access to the VM nodes

In single tier infrastructure you access directly the individual VM nodes via SSH on public IP addresses.
Two tier infrastructure requires the access following way:
1. Establish the connection with bastion
```sh
sshuttle -r ubuntu@<bastion-ip>
```
1. Connect directly to VM nodes via SSH on private IP addresses:
```sh
ssh  ubuntu@<vm-node-ip-from-10.10.10.0/24>
```

## Notes
1. Scalability test (this demo) can be executed in dedicated project (`meta-cloud-scalability-test`) only.
1. Always create infrastructure in few steps, see [toplevel module count description](https://gitlab.ics.muni.cz/cloud/g2/openstack-infrastructure-as-code-automation/-/blob/master/clouds/g2/ostrava/scalability-test/terraform/main.tf#L7) for more details.