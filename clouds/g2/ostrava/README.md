# General IaaS infrastructure demo

Assuming you are allowed to use e-INFRA CZ OpenStack cloud in Ostrava and you can log in via [e-INFRA CZ OpenStack cloud dashboard](https://ostrava.openstack.cloud.e-infra.cz/) using one of supported federations (e-INFRA CZ, ...).

We recommend to build custom cloud infrastructure with Terraform or openstack client rather than using [e-INFRA CZ OpenStack cloud Horizon UI dashboard](https://horizon.ostrava.openstack.cloud.e-infra.cz).

Below demos show in detail how to do so.

## [Terraform `general` demo](./terraform)

Terraform demo shows how to automate building highly scalable IaaS infrastructure.

## [OpenStack client `general` demo](./commandline)

OpenStack shell script demo shows how to automate small IaaS infrastructure which does not need additional scalability.
