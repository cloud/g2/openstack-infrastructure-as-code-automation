terraform {
  backend "local" {}
}

module "toplevel" {
  # infrastructure type:
  # -------------------------------------------------------------------------
  # two tier infrastructure (2tier_public_bastion_private_vm_farm module):
  # * single public facing tiny bastion VM
  # * <nodes_count> private HPC VM farm
  source                  = "./modules/2tier_public_bastion_private_vm_farm"
  # single tier infrastructure (1tier_public_vm_farm monule)
  # * <nodes_count> public HPC VM farm
  #source                  = "./modules/1tier_public_vm_farm"

  infra_name              = "general-tf-demo"

  nodes_count             = 2
  nodes_image             = "ubuntu-jammy-x86_64"
  nodes_extra_volume_size = 10  # extra volume size in GB

  # Externally defined parameters (see *.tfvar files)
  # -------------------------------------------------------------------------
  # root variables wired 1:1 to "toplevel" module to be able to toggle between
  # group and personal project infrastructure in various clouds
  internal_network_creation_enable = var.internal_network_creation_enable
  internal_network_name            = var.internal_network_name
  internal_subnet_creation_enable  = var.internal_subnet_creation_enable
  internal_subnet_name             = var.internal_subnet_name
  router_creation_enable           = var.router_creation_enable
  public_external_network          = var.public_external_network # "external-ipv4-general-public"
  nodes_flavor                     = var.nodes_flavor # "e1.small"
  bastion_flavor                   = var.bastion_flavor # "e1.small"
}
