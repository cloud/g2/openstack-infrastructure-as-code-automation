#!/usr/bin/env bash
# e-INFRA CZ G2 openstack terraform demo - personal/group project
# Usage: terraform-demo.sh <cloud-environment-name>


SCRIPT_DIR=$(dirname $(readlink -f $0))
#############################################################################
# variables
#############################################################################
CLOUD_ENV_NAME="$1"

export CLOUD_ENV_NAME

#############################################################################
# functions
#############################################################################
source ${SCRIPT_DIR}/../../lib.sh.inc

#############################################################################
# main steps
#############################################################################
log_section "Using commandline tools:"
./terraform.sh validate-tools

log_section "Detect cloud (${CLOUD_ENV_NAME} requested):"
./terraform.sh detect-cloud

log_section "Detect project:"
./terraform.sh detect-project

log_section "Initialize the terraform environment:"
./terraform.sh init

log_section "Validate the terraform environment:"
./terraform.sh validate

log_section "Perform terraform environment planning:"
./terraform.sh plan --out plan

log_section "Apply previously performed terraform plan i.e. create IaaS infrastructure:"
./terraform.sh apply plan

log_section "Destroy previously created IaaS infrastructure:"
./terraform.sh destroy

