#########################
# general configuration #
#########################
variable "infra_name" {
  description = "Infrastructure (profile) name. Used as a name prefix. Must match [a-zA-Z0-9-]+ regexp."
  default     = "general-tf-demo"
}

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa.pub"
}

variable "internal_network_cidr" {
  description = "Internal network address, use CIDR notation"
  default     = "10.10.10.0/24"
}

variable "public_external_network" {
  description = "Cloud public external network pool"
}

variable "router_creation_enable" {
  description = "Create dedicated router instance. true/false ~ create new / reuse existing personal router"
  default     = true
}

variable "internal_network_creation_enable" {
  description = "Create dedicated internal network. true/false ~ create new / reuse existing personal network"
  default     = true
}

variable "internal_network_name" {
  description = "Internal network name. Either dedicated new network or existing personal network name"
  default     = "<var.infra_name>_network"
}

variable "internal_subnet_creation_enable" {
  description = "Create dedicated subnet instance. true/false ~ create new / reuse existing personal subnet"
  default     = true
}

variable "internal_subnet_name" {
  description = "Internal network subnet name. Either dedicated new subnet or existing personal subnet name"
  default     = "<var.infra_name>_subnet"
}

####################
# bastion settings #
####################
variable "bastion_name" {
  description = "Name of the bastion VM. Must match [a-zA-Z0-9-]+ regexp."
  default = "bastion-server"
}

variable "bastion_flavor" {
  description = "Bastion VM - flavor name"
  default = "e1.tiny"
}

variable "bastion_image" {
  description = "Bastion OS: Image name"
  default     = "ubuntu-jammy-x86_64"
}

variable "bastion_ssh_user_name" {
  default = "ubuntu"
}

#########################
# master nodes settings #
#########################

variable "nodes_count" {
  default = 1
}

variable "nodes_name" {
  description = "Name of the nodes. Must match [a-zA-Z0-9-]+ regexp."
  default = "server"
}

variable "nodes_flavor" {
  description = "Nodes - flavor name"
  default = "standard.large"
}

variable "nodes_image" {
  description = "nodes OS: Image name"
  default     = "ubuntu-jammy-x86_64"
}

variable "nodes_ssh_user_name" {
  default = "ubuntu"
}

variable "nodes_volume_size" {
  description = "The size of the volume to create (in gigabytes) for root filesystem. "
  default     = "10"
}

variable "nodes_extra_volume_size" {
  description = "The size of the volume to create (in gigabytes) for extra data. 0 to disable extra volume."
  default     = "10"
}

variable "nodes_extra_volume_type" {
  description = "The type of extra volume."
  default     = null
}

