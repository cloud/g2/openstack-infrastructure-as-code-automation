#!/usr/bin/env bash
# terraform wrapper for automatic personal/group project reconfiguration
# terraform.sh <arguments>
#
# <arguments> are:
# * detect-cloud
# * detect-project
# * validate-tools
# * clean-up-metadata-and-state-files
# * ... or any valid terraform arguments

# variables
TERRAFORM="terraform"
SCRIPT_DIR=$(dirname $(readlink -f $0))
# functions
source ${SCRIPT_DIR}/../../lib.sh.inc


if [[ "$1" =~ (detect-(cloud|project)|validate-tools|clean-up-metadata-and-state-files) ]]; then
    if [ "$1" == "detect-cloud" ]; then
        log "Using OpenStack cloud:"
        openstack version show -fcsv | grep identity
    elif [ "$1" == "clean-up-metadata-and-state-files" ]; then
        log "Cleaning up metadata and state files:"
        rm -rf ./.terraform
        rm -f ./.terraform* ./terraform.tfstate*
    elif [ "$1" == "detect-project" ]; then
        project_type=group
        if prj_name=$(is_personal_project); then
            project_type=personal
        fi
        log "Using OpenStack ${project_type} project named: ${prj_name}"
    elif [ "$1" == "validate-tools" ]; then
        log "Using commandline tools:"
        report_tools "terraform version"
    fi
    exit $?
else
    project_type=group
    if prj_name=$(is_personal_project); then
        project_type=personal
    fi
    if [ "$1" == "plan" -o "$1" == "destroy" ]; then
        if [ -n "${CLOUD_ENV_NAME}" ]; then
            ${TERRAFORM} "$@" "--var-file=${CLOUD_ENV_NAME}-${project_type}-projects.tfvars"
        else
            ${TERRAFORM} "$@"
        fi
    else
        ${TERRAFORM} "$@"
    fi
fi

