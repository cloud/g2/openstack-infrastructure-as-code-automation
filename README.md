# OpenStack Infrastructure as Code automation

Examples of an infrastructure definition using openstack commandline project and terraform infrastructure description.

## [G2 e-INFRA Ostrava cloud general demonstrations](/clouds/g2/ostrava/general/README.md)
   * [shell and OpenStack command-line client](clouds/g2/ostrava/general/commandline)
   * [terraform declarative language](clouds/g2/ostrava/general/terraform)

## [G1 MetaCentrum/e-INFRA Brno general demonstrations](/clouds/g1/brno/general/README.md)
   * [infrastructure in terraform HCL](clouds/g1/brno/general/terraform) (IaC, GitOps, DevOps)
   * [shell and OpenStack command-line client](clouds/g1/brno/general/commandline)

## [G1 Brno vo.enes.org demonstrations](/clouds/g1/brno/vo.enes.org/README.md)
   * [infrastructure in terraform HCL](clouds/g1/brno/vo.enes.org/terraform) (IaC, GitOps, DevOps)
   * [shell and OpenStack command-line client](clouds/g1/brno/vo.enes.org/commandline)

## [G1 Brno Repet terraform Workshop](/clouds/g1/brno/repet-workshop/terraform/ost-terraform/README.md)

## [G1 Brno Metaseminar hands-on](/clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/README.md)
   * [shell and OpenStack command-line client](clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/commandline)
   * [terraform declarative language](clouds/g1/brno/meta-metaseminar-hands-on-2023-04-28/terraform_group_project)

## [G2 Ostrava cloud announcement demo](/clouds/g2/ostrava)
   * [shell and OpenStack command-line client](clouds/g2/ostrava/general/commandline)
   * [terraform declarative language](clouds/g2/ostrava/general/terraform)

